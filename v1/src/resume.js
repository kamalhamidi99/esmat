'use strict';

class ResumeContent extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="content-wrap">
				<div id="resume" className="inner-content">
					<section id="page-title" className="inner-section ui-menu-color02">
						<div className="container-fluid nopadding">
							<h2 className="font-accident-two-light color01 uppercase"
								data-animation-origin="right"
								data-animation-duration="400"
								data-animation-delay="100"
								data-animation-distance="50px">
								Resume
							</h2>
							{/* <h4 className="font-accident-two-normal color01 uppercase subtitle"
								data-animation-origin="right"
								data-animation-duration="400"
								data-animation-delay="200"
								data-animation-distance="50px">
								A little thing about me:
							</h4>
							<p className="small color01"
								data-animation-origin="right"
								data-animation-duration="400"
								data-animation-delay="300"
								data-animation-distance="50px">
								Hello, my name is Esmat. I`m a developer, I can build web, mobile and windows app using c# and dot net tools. I like to read books, watch movies...
							</p> */}
						</div>
					</section>
					<section className="inner-section light bg-color01">
						<div className="container-fluid nopadding">
							<div data-animation-origin="top"
								data-animation-duration="200"
								data-animation-delay="300"
								data-animation-distance="20px">
								<h3 className="font-accident-two-normal uppercase text-center">Facts About Me</h3>
								<div className="dividewhite1"></div>
								<p className="small text-center fontcolor-medium">
									Here are some things that you should know about me:
								</p>
								<div className="dividewhite4"></div>
							</div>
							<div className="row">
								<div className="col-md-6"
									data-animation-origin="top"
									data-animation-duration="400"
									data-animation-delay="600"
									data-animation-distance="30px">
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Status:</span></div>
										<div className="col-sm-8"><p className="">Open to Offers</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Desired Salary:</span></div>
										<div className="col-sm-8"><p className="">To be Discussed</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Education Level:</span></div>
										<div className="col-sm-8"><p className="">Bachelor’s Degree, Computer Software Engineering</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Job Type:</span></div>
										<div className="col-sm-8"><p className="">Contract, Freelance</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Mobility:</span></div>
										<div className="col-sm-8"><p className="">Ready to Relocate</p></div>
									</div>
								</div>
								<div className="col-md-6"
									data-animation-origin="top"
									data-animation-duration="400"
									data-animation-delay="800"
									data-animation-distance="30px">
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Languages:</span></div>
										<div className="col-sm-8"><p className="">English, Persian</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Position:</span></div>
										<div className="col-sm-8"><p className="">Senior Programmer</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Industry:</span></div>
										<div className="col-sm-8"><p className="">Web Development</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Speciality:</span></div>
										<div className="col-sm-8"><p className="">Web, Mobile and Windows Development and Design</p></div>
									</div>
									<div className="row">
										<div className="col-sm-4"><span className="font-accident-two-bold uppercase">Hobbies:</span></div>
										<div className="col-sm-8"><p className="">Books, Movies, Blogs</p></div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section id="m-details" className="inner-section light bg-color02">
						<div className="container-fluid nopadding">
							<div data-animation-origin="top"
								data-animation-duration="100"
								data-animation-delay="300"
								data-animation-distance="30px">
								<h3 className="font-accident-two-normal uppercase text-center">Personal qualities</h3>
								<div className="dividewhite1"></div>
								{/* <p className="small text-center fontcolor-medium">
									With the Gridus personal theme it’s possible to make an impressive website
									in just 15 minutes.
								</p> */}
								<div className="dividewhite6"></div>
							</div>
							<div className="row">
								<div className="col-md-3 infoblock text-center"
									data-animation-origin="left"
									data-animation-duration="400"
									data-animation-delay="400"
									data-animation-distance="50px">
									<div className="row">
										<div className="col-md-12"><i className="flaticon-photo246"></i></div>
										<div className="col-md-12">
											<div className="dividewhite1"></div>
											<h5 className="font-accident-two-bold uppercase">Clean Code</h5>
											<p className="small">
												Write clean, well-organized, well-documented code that meets company standards.
											</p>
										</div>
									</div>
									<div className="divider-dynamic"></div>
								</div>
								<div className="col-md-3 infoblock text-center"
									data-animation-origin="left"
									data-animation-duration="400"
									data-animation-delay="200"
									data-animation-distance="25px">
									<div className="row">
										<div className="col-md-12"><i className="flaticon-pie-graph"></i></div>
										<div className="col-md-12">
											<div className="dividewhite1"></div>
											<h5 className="font-accident-two-bold uppercase">Optimized</h5>
											<p className="small">
												Ability to create mobile optimized and user-friendly website designs, ensuring ease of navigation, well organized content.
											</p>
										</div>
									</div>
									<div className="divider-dynamic"></div>
								</div>
								<div className="col-md-3 infoblock text-center"
									data-animation-origin="right"
									data-animation-duration="400"
									data-animation-delay="200"
									data-animation-distance="25px">
									<div className="row">
										<div className="col-md-12"><i className="flaticon-clocks18"></i></div>
										<div className="col-md-12">
											<div className="dividewhite1"></div>
											<h5 className="font-accident-two-bold uppercase">Customizable</h5>
											<p className="small">
												The Ready-to-use pre-defined color css classNamees make possible to Customize your websire in a wide range.
											</p>
										</div>
									</div>
									<div className="divider-dynamic"></div>
								</div>
								<div className="col-md-3 infoblock text-center"
									data-animation-origin="right"
									data-animation-duration="400"
									data-animation-delay="400"
									data-animation-distance="50px">
									<div className="row">
										<div className="col-md-12"><i className="flaticon-stats47"></i></div>
										<div className="col-md-12">
											<div className="dividewhite1"></div>
											<h5 className="font-accident-two-bold uppercase">Experience</h5>
											<p className="small">
												Over 6 years of experience in the fields of Web and desktop programming
											</p>
										</div>
									</div>
									<div className="divider-dynamic"></div>
								</div>
							</div>
						</div>
					</section>
					<section id="timeline" className="light inner-section">
						<div className="container-fluid nopadding">
							<div className="text-center"
								data-animation-origin="top"
								data-animation-duration="400"
								data-animation-delay="400"
								data-animation-distance="30px">
								<div className="dividewhite4"></div>
								<h3 className="font-accident-two-normal uppercase">Timeline</h3>
								<h5 className="font-accident-two-normal uppercase subtitle">My Work Experience and Education</h5>
								<div className="dividewhite1"></div>
								<p className="small fontcolor-medium">
									I graduated in computer science in 2013 and started working after that. Here is my timeline so far.
								</p>
							</div>
							<div className="dividewhite4"></div>
							<ul className="timeline-vert timeline">
								<li className="timeline-inverted">
									<div className="timeline-badge primary"><i className="flaticon-clocks18"></i></div>
									<div className="timeline-panel"
										data-animation-origin="left"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2015 - Persent</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Programmer</h4>
											<h6 className="uppercase">at Solutions-it Drenthe (Netherlands)</h6>
											<p>
												In 2015, I met Mr. Hossen Bazian, who is the CEO of Solutions-it Drenthe in the Netherlands. He suggested to me to work for him as telecommunications and project-based contracts.
											</p>
										</div>
									</div>
								</li>
								<li>
									<div className="timeline-badge primary"><i className="flaticon-clocks18"></i></div>
									<div className="timeline-panel"
										data-animation-origin="left"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2015</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Certification</h4>
											<h6 className="uppercase">Secure Coding in .NET: Developing Defensible Applications</h6>
											<p>
												In 2015, I went to private school to learn more about the security of web applications and got my second Certification in there.
											</p>
										</div>
									</div>
								</li>
								<li className="timeline-inverted">
									<div className="timeline-badge primary"><i className="flaticon-clocks18"></i></div>
									<div className="timeline-panel"
										data-animation-origin="left"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2014 - Persent</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Senior Programmer</h4>
											<h6 className="uppercase">at Tejarat Pardazan Electronic Company</h6>
											<p>
												In 2014, I changed my workplace and got haired in Tejarat Pardazan Electronic. In there I learned a lot of new stuff and quickly grew fast.
											</p>
										</div>
									</div>
								</li>
								<li>
									<div className="timeline-badge success"><i className="flaticon-clocks18"></i></div>
									<div className="timeline-panel"
										data-animation-origin="right"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2013 - 2014</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Programmer</h4>
											<h6 className="uppercase">at Pasha Sanat Yaran Company</h6>
											<p>
												In 2013, After I finished my study, I started to word at Pasha Sanat Yaran Company as a front-end and backend.
											</p>
										</div>
									</div>
								</li>
								<li className="timeline-inverted">
									<div className="timeline-badge success"><i className="flaticon-clocks18"></i></div>
									<div className="timeline-panel"
										data-animation-origin="right"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2013</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Certifications</h4>
											<h6 className="uppercase">Engineering in enterprise web development with asp.net</h6>
											<p>
												In 2013, I felt that I needed more training in web development, so I took classNamees in the `Technical & Vocational Training Organization` and got my first certificate in `Engineering in enterprise web development with asp.net` with a score of 88/100.
											</p>
										</div>
									</div>
								</li>
								<li>
									<div className="timeline-badge danger"><i className="flaticon-graduation61"></i></div>
									<div className="timeline-panel"
										data-animation-origin="left"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2010 - 2013</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Bachelor’s Degree</h4>
											<h6 className="uppercase">Computer Software Engineering</h6>
											<p>
												In 2010, After associate's degree, I took the entrance exam and entered the faculty of Karun Higher Education Institute for my bachelor degree.
											</p>
										</div>
									</div>
								</li>
								<li className="timeline-inverted info">
									<div className="timeline-badge warning"><i className="flaticon-graduation61"></i></div>
									<div className="timeline-panel"
										data-animation-origin="right"
										data-animation-duration="400"
										data-animation-delay="400"
										data-animation-distance="25px">
										<p className="timeline-time fontcolor-invert">2008 - 2010</p>
										<div className="timeline-photo"></div>
										<div className="timeline-heading">
											<h4 className="font-accident-two-normal uppercase">Associate's Degree</h4>
											<h6 className="uppercase">Computer Science</h6>
											<p>
												In 2010, I got my associate's degree from Islamic Azad University.
											</p>
										</div>
									</div>
								</li>
							</ul>
							<div className="text-center">
								<a className="btn btn-darker">Start</a>
							</div>
							<div className="dividewhite6"></div>
						</div>
					</section>
				</div>
			</div>
		);
	}
}
let domContainer = document.querySelector('#containerBody');
ReactDOM.render(<ResumeContent />, domContainer);