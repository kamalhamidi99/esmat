'use strict';

class HomeContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="content-wrap">
                <section className="container-fluid" data-section="home">
                    <div className="row flex-row">
                        <div id="personal-2" className="col-md-4 flex-column ui-block-color01 light nopadding"
                            data-animation-origin="right" data-animation-duration="300" data-animation-delay="600"
                            data-animation-distance="200px">
                            <div className="row flex-panel nopadding">
                                <div className="col-md-12 nopadding">
                                    <div className="row flex-row nopadding">
                                        <span className="col-md-6 flex-column bg-color02 p-grid-item nopadding hvr-bounce-to-top hvr-green">
                                            <div className="flex-panel padding-44">
                                                <div className="p-icon green"></div>
                                                <span className="uppercase">Status</span>
                                                <p className="">Open to offers</p>
                                            </div>
                                        </span>
                                        <span className="col-md-6 flex-column bg-color03 p-grid-item nopadding hvr-bounce-to-top hvr-blue">
                                            <div className="flex-panel padding-44">
                                                <div className="p-icon blue"></div>
                                                <span className="uppercase">Desired Salary</span>
                                                <p className="">To be Discussed</p>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div className="col-md-12 nopadding">
                                    <div className="row flex-row nopadding">
                                        <span className="col-md-6 flex-column bg-color03 p-grid-item nopadding hvr-bounce-to-top hvr-red">
                                            <div className="flex-panel padding-44">
                                                <div className="p-icon red"></div>
                                                <span className="uppercase">Job Type</span>
                                                <p className="">Contract, Freelance</p>
                                            </div>
                                        </span>
                                        <span className="col-md-6 flex-column bg-color02 p-grid-item nopadding hvr-bounce-to-top hvr-yellow">
                                            <div className="flex-panel padding-44">
                                                <div className="p-icon yellow"></div>
                                                <span className="uppercase">Mobility</span>
                                                <p className="">Ready to Relocate</p>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                                <div className="col-md-12 nopadding">
                                    <div className="row flex-row nopadding">
                                        <span className="col-md-6 flex-column bg-color02 p-grid-item nopadding hvr-bounce-to-top hvr-blue">
                                            <div className="flex-panel padding-44">
                                                <div className="p-icon blue"></div>
                                                <span className="uppercase">Position</span>
                                                <p className="">Senior Programmer</p>
                                            </div>
                                        </span>
                                        <span className="col-md-6 flex-column bg-color03 p-grid-item nopadding hvr-bounce-to-top hvr-orange">
                                            <div className="flex-panel padding-44">
                                                <div className="p-icon orange"></div>
                                                <span className="uppercase">Industry</span>
                                                <p className="">Web Development</p>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="details" className="col-md-8 flex-column bg-color01 light nopadding" data-animation-origin="left"
                            data-animation-duration="300" data-animation-delay="400" data-animation-distance="200px">
                            <div className="padding-50 flex-panel">
                                <div className="row row-no-padding">
                                    <div className="col-md-12 nopadding">
                                        <h3 className="font-accident-two-normal uppercase">A little thing about me</h3>
                                        <div className="quote">
                                            {/* <h5 className="font-accident-two-medium uppercase subtitle">Perfect for CV / Resume or
                                            Portfolio Website</h5> */}
                                            {/*  <div className="dividewhite3"></div> */}
                                        </div>
                                    </div>
                                </div>
                                <div className="divider-dynamic"></div>
                                <div className="row nopadding">
                                    <div className="col-md-12 infoblock nopadding">
                                        <span>Hello, my name is Esmat. I'm 31 years old. I have 6 years of professional experience. I work as a web developer, also along the way, I learned to build windows and mobile applications.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="professional" className="container-fluid" data-section="home">
                    <div className="row flex-row">
                        <div id="pro-experience" className="col-md-4 flex-column dark nopadding ui-block-color02 flex-col"
                            data-animation-origin="bottom" data-animation-duration="300" data-animation-delay="800"
                            data-animation-distance="200px">
                            <div className="padding-50 flex-panel">
                                <h3 className="font-accident-two-normal uppercase">Employment</h3>
                                <div className="dividewhite4"></div>
                                <div className="experience">
                                    <ul className="">
                                        <li className="date">2016 - Present</li>
                                        <li className="company uppercase">
                                            <a href="https://www.solutions-it.nl/" target="_blank">
                                                Solutions-it Drenthe
                                            </a>
                                            <small style={{ fontSize: '10px' }}> (Netherlands) </small>
                                        </li>
                                        <li className="position">Programmer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Windows App & Web Developer
                                    </li>
                                    </ul>
                                    <ul className="">
                                        <li className="date">2014 - Present</li>
                                        <li className="company uppercase">
                                            <a href="http://tpe.co.ir/tejarat-pardazan-electronic/" target="_blank">
                                                Tejarat Pardazan Electronic
                                            </a>
                                            <small style={{ fontSize: '10px' }}> (Iran) </small>
                                        </li>
                                        <li className="position">Senior Programmer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Web Developer & Mobile Developer
                                    </li>
                                    </ul>
                                    <ul className="">
                                        <li className="date">2013 - 2014</li>
                                        <li className="company uppercase">
                                            <a>
                                                Pasha Sanat Yaran Company
                                            </a>
                                            <small style={{ fontSize: '10px' }}> (Iran) </small>
                                        </li>
                                        <li className="position">Programmer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Web Developer & Web Designer
                                        </li>
                                    </ul>
                                    <ul>
                                        <li className="date">2014 - Present</li>
                                        <li className="company uppercase">
                                            <a>
                                                Freelancer
                                            </a>
                                        </li>
                                        <li className="position">Developer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Web Developer & Mobile Developer
                                        </li>
                                    </ul>
                                </div>
                                <a data-content="resume" className="btn btn-wh-trans btn-xs gototop">Learn More</a>
                                <div className="dividewhite1"></div>
                            </div>
                        </div>
                        <div id="bar-skills" className="col-md-8 flex-column dark nopadding ui-block-color03 flex-col"
                            data-section="bar-skills" data-animation-origin="right" data-animation-duration="400"
                            data-animation-delay="1100" data-animation-distance="200px">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="padding-50 flex-panel" style={{ paddingBottom: '20px' }}>
                                        <h3 className="font-accident-two-normal uppercase">Professional skills</h3>
                                        <div className="dividewhite2"></div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <h5 className="font-accident-two-medium uppercase letter-spacing-03">Web Development</h5>
                                                <div className="progress">
                                                    <div className="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90"
                                                        aria-valuemin="0" aria-valuemax="100">
                                                        90%
                                                    </div>
                                                </div>
                                                <h5 className="font-accident-two-medium uppercase letter-spacing-03">Mobile Development</h5>
                                                <div className="progress">
                                                    <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="70"
                                                        aria-valuemin="0" aria-valuemax="100">
                                                        70%
                                                    </div>
                                                </div>
                                                <h5 className="font-accident-two-medium uppercase letter-spacing-03">Windows Development
                                                </h5>
                                                <div className="progress">
                                                    <div className="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80"
                                                        aria-valuemin="0" aria-valuemax="100">
                                                        80%
                                                    </div>
                                                </div>
                                                <h5 className="font-accident-two-medium uppercase letter-spacing-03">Designer</h5>
                                                <div className="progress">
                                                    <div className="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="75"
                                                        aria-valuemin="0" aria-valuemax="100">
                                                        75%
                                                </div>
                                                </div>
                                                <div className="dividewhite1"></div>
                                                <div id="circle-skills" className="flex-column dark nopadding ui-block-color03 flex-col"
                                                    data-section="progress" data-animation-origin="right" data-animation-duration="400"
                                                    data-animation-delay="1100" data-animation-distance="200px">
                                                    <div className="">
                                                        <div className="row">
                                                            <div className="col-md-3 col-sm-3 col-xs-12 nopadding">
                                                                <div className="progressbar text-center">
                                                                    <div id="circle1" data-progressbar="circle"
                                                                        data-progressbar-color="#fff" data-progressbar-trailcolor="#fff"
                                                                        data-progressbar-to='{"color": "#51f2ec", "width": 4}'
                                                                        data-progressbar-from='{"color": "#3498db", "width": 4}'
                                                                        data-progressbar-value="0.85">
                                                                    </div>
                                                                    <h4 className="font-accident-two-bold uppercase">C#</h4>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-3 col-sm-3 col-xs-12 nopadding">
                                                                <div className="progressbar text-center">
                                                                    <div id="circle2" data-progressbar="circle"
                                                                        data-progressbar-color="#fff" data-progressbar-trailcolor="#fff"
                                                                        data-progressbar-to='{"color": "#ffd200", "width": 4}'
                                                                        data-progressbar-from='{"color": "#3498db", "width": 4}'
                                                                        data-progressbar-value="0.80">
                                                                    </div>
                                                                    <h4 className="font-accident-two-bold uppercase">SQL</h4>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-3 col-sm-3 col-xs-12 nopadding">
                                                                <div className="progressbar text-center">
                                                                    <div id="circle3" data-progressbar="circle"
                                                                        data-progressbar-color="#fff" data-progressbar-trailcolor="#fff"
                                                                        data-progressbar-to='{"color": "#F09C88", "width": 4}'
                                                                        data-progressbar-from='{"color": "#3498db", "width": 4}'
                                                                        data-progressbar-value="0.75">
                                                                    </div>
                                                                    <h4 className="font-accident-two-bold uppercase">Javascript</h4>
                                                                </div>
                                                            </div>
                                                            <div className="col-md-3 col-sm-3 col-xs-12 nopadding">
                                                                <div className="progressbar text-center">
                                                                    <div id="circle4" data-progressbar="circle"
                                                                        data-progressbar-color="#fff" data-progressbar-trailcolor="#fff"
                                                                        data-progressbar-to='{"color": "#5cb85c", "width": 4}'
                                                                        data-progressbar-from='{"color": "#3498db", "width": 4}'
                                                                        data-progressbar-value="0.95">
                                                                    </div>
                                                                    <h4 className="font-accident-two-bold uppercase">HTML/CSS</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-12">
                                    <div id="clients" className="light padding-50" data-animation-origin="bottom" data-animation-duration="500" data-animation-delay="500" data-animation-distance="50px" style={{ visibility: 'visible' }}>
                                        <div className="row row-no-padding">
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Bootstrap"><img src="../../assets/custom/2.2.2/images/layouts/all/bootstrap.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Gitlab"><img src="../../assets/custom/2.2.2/images/layouts/all/gitlab.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Highcharts"><img src="../../assets/custom/2.2.2/images/layouts/all/highcharts.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="jQuery"><img src="../../assets/custom/2.2.2/images/layouts/all/jquery.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="LeafletJs"><img src="../../assets/custom/2.2.2/images/layouts/all/leaflet.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Adobe Photoshop"><img src="../../assets/custom/2.2.2/images/layouts/all/photoshop.jpg" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="ReactJs"><img src="../../assets/custom/2.2.2/images/layouts/all/reactjs.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Stimulsoft Reports"><img src="../../assets/custom/2.2.2/images/layouts/all/stimulsoft.jpg" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Xamarin Forms"><img src="../../assets/custom/2.2.2/images/layouts/all/xamarin.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Adobe Xd"><img src="../../assets/custom/2.2.2/images/layouts/all/xd.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Visual Studio Code"><img src="../../assets/custom/2.2.2/images/layouts/all/code.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Asp.net Mvc"><img src="../../assets/custom/2.2.2/images/layouts/all/mvc.png" className="img-responsive" /></a></div>
                                        </div>
                                        <div className="row row-no-padding" style={{ marginTop: '15px' }}>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Asp.net Core"><img src="../../assets/custom/2.2.2/images/layouts/all/core.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Sql Server Management Studio"><img src="../../assets/custom/2.2.2/images/layouts/all/ssms.png" className="img-responsive" /></a></div>
                                            <div className="col-md-1 col-sm-2 col-xs-4"><a title="Redis"><img src="../../assets/custom/2.2.2/images/layouts/all/redis.png" className="img-responsive" /></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}
let domContainer = document.querySelector('#containerBody');
ReactDOM.render(<HomeContent />, domContainer);