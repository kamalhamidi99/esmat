'use strict';

class ProtfolioCarContent extends React.Component {
   constructor(props) {
      super(props);
   }

   render() {
      return (
         <div className="content-wrap">
            <div id="portfolio-item-page" className="inner-content">
               <section className="inner-section">
                  <div className="post-header"
                     data-animation-origin="right"
                     data-animation-duration="400"
                     data-animation-delay="100"
                     data-animation-distance="50px">
                     <h2 className="font-accident-two-normal">Arvand Cars Management</h2>
                     <p>This system builds to manage all cars that registered in the Arvand free zone.</p>
                     <div className="dividewhite1"></div>
                  </div>
                  <div className="dividewhite4"></div>
                  <div className="row">
                     <div className="col-md-4"
                        data-animation-origin="top"
                        data-animation-duration="400"
                        data-animation-delay="400"
                        data-animation-distance="50px">
                        <div className="row">
                           <div className="col-md-12 col-sm-6 col-xs-12">
                              <img src="./assets/images/car/1.jpg" className="img-responsive radius-4" alt="portfolio item" />
                              <div className="dividewhite2"></div>
                           </div>
                           <div className="col-md-12 col-sm-6 col-xs-12">
                              <div className="popup-call gallery-group-item" data-group="car">
                                 <a href="./assets/images/car/1.jpg" className="gallery-item btn btn-default">View All</a>
                                 <a href="./assets/images/car/2.jpg" className="gallery-item"></a>
                                 <a href="./assets/images/car/3.jpg" className="gallery-item"></a>
                                 <a href="./assets/images/car/4.jpg" className="gallery-item"></a>
                                 <a href="./assets/images/car/5.jpg" className="gallery-item"></a>
                              </div>
                           </div>
                        </div>
                        <div className="dividewhite6"></div>
                     </div>
                     <div id="portfolio-overview" className="col-md-8"
                        data-animation-origin="top"
                        data-animation-duration="400"
                        data-animation-delay="600"
                        data-animation-distance="50px">
                        <h3 className="font-accident-two-light">Project Data</h3>
                        <div className="dividewhite4"></div>
                        <div className="portfolio-item-details table">
                           <div className="row table-row">
                              <div className="table-cell">Categories:</div>
                              <div className="table-cell"><span>Web</span></div>
                           </div>
                           <div className="row table-row">
                              <div className="table-cell">Author:</div>
                              <div className="table-cell"><span>Esmat Zilaei & Kamal Hamidi</span></div>
                           </div>
                           <div className="row table-row">
                              <div className="table-cell">Client:</div>
                              <div className="table-cell"><span>Arvand Free Zone</span></div>
                           </div>
                           <div className="row table-row">
                              <div className="table-cell">Url:</div>
                              <div className="table-cell"><span><a href="https://car.arvandfreezone.com" target="_blank">https://car.arvandfreezone.com</a></span></div>
                           </div>
                        </div>
                        <div className="dividewhite4"></div>
                        <p>
                           The system is designed to manage all the cars of the Arvand license plate and applicants can view or
                           request all of their car related issues in their profile. More than 100,000 cars are in the system.
                        </p>
                        <div className="dividewhite4"></div>
                        <div className="row">
                           <div className="col-md-4">
                              <h3 className="font-accident-two-light">Technology</h3>
                              <div className="dividewhite2"></div>
                              <ul className="portfolio-item-details">
                                 <li>Asp.net Mvc</li>
                                 <li>C#</li>
                                 <li>Sql Server</li>
                                 <li>Bootstrap</li>
                                 <li>Stimulsoft</li>
                                 <li>Highcharts</li>
                              </ul>
                              <div className="dividewhite4"></div>
                           </div>
                           <div className="col-md-8">
                              <h3 className="font-accident-two-light">Share</h3>
                              <div className="dividewhite2"></div>
                              <div className="share">
                                 <ul className="list-inline social">
                                    <li><a target="_blank" href="#" className="rst-icon-facebook"><i className="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="#" className="rst-icon-twitter"><i className="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" href="#" className="rst-icon-pinterest"><i className="fa fa-pinterest"></i></a></li>
                                    <li><a target="_blank" href="#" className="rst-icon-instagram"><i className="fa fa-instagram"></i></a></li>
                                    <li><a target="_blank" href="#" className="rst-icon-google"><i className="fa fa-google-plus"></i></a></li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="dividewhite8"></div>
               </section>
            </div>
         </div>
      );
   }
}
let domContainer = document.querySelector('#containerBody');
ReactDOM.render(<ProtfolioCarContent />, domContainer);