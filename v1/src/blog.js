'use strict';

class BlogContent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="content-wrap">
                <div id="blog" className="inner-content" style={{ minHeight: 'auto' }}>
                    <section id="page-title" className="inner-section ui-menu-color06">
                        <div className="container-fluid nopadding">
                            <h2 className="font-accident-two-light color01 uppercase"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="100"
                                data-animation-distance="50px">Blog</h2>
                            <h4 className="font-accident-two-normal color01 uppercase subtitle"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="200"
                                data-animation-distance="50px">Coming Soon</h4>
                        </div>
                    </section>
                </div>
            </div>
        );
    }
}
let domContainer = document.querySelector('#containerBody');
ReactDOM.render(<BlogContent />, domContainer);