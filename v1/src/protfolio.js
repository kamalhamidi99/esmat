'use strict';

class ProtfolioContent extends React.Component {
   constructor(props) {
      super(props);
   }

   render() {
      return (
         <div className="content-wrap">
            <div id="portfolio" className="inner-content">
               <section id="page-title" className="inner-section ui-menu-color03">
                  <div className="container-fluid nopadding">
                     <h2 className="font-accident-two-light color01 uppercase"
                        data-animation-origin="right"
                        data-animation-duration="400"
                        data-animation-delay="100"
                        data-animation-distance="50px">Portfolio</h2>
                     {/*  <h4 className="font-accident-two-normal color01 uppercase subtitle"
                        data-animation-origin="right"
                        data-animation-duration="400"
                        data-animation-delay="200"
                        data-animation-distance="50px">Made for Creative Professionals</h4> */}
                     {/*                      <p className="small color01"
                        data-animation-origin="right"
                        data-animation-duration="400"
                        data-animation-delay="300"
                        data-animation-distance="50px">
                        I love to make new apps,
                        </p> */}
                  </div>
               </section>
               <section id="counts" className="light inner-section bg-color02" data-section="counter">
                  <div className="container-fluid nopadding">
                     <div className="count-container row">
                        <div className="col-lg-3 col-sm-6 col-xs-12 count">
                           <div
                              data-animation-origin="top"
                              data-animation-duration="300"
                              data-animation-delay="200"
                              data-animation-distance="35px">
                              <div className="count-icon">
                                 <i class="fa fa-chrome"></i>
                              </div>
                              <span className=".integers digit font-accident-two-normal web" data-counter="0">0</span>

                              <div className="count-text font-accident-two-bold">Websites</div>
                           </div>
                        </div>
                        <div className="col-lg-3 col-sm-6 col-xs-12 count">
                           <div
                              data-animation-origin="top"
                              data-animation-duration="300"
                              data-animation-delay="400"
                              data-animation-distance="35px">
                              <div className="count-icon">
                                 <i className="fa fa-mobile"></i>
                              </div>
                              <span className=".integers digit font-accident-two-normal mobile" data-counter="0">0</span>

                              <div className="count-text font-accident-two-bold">Mobile</div>
                           </div>
                        </div>
                        <div className="col-lg-3 col-sm-6 col-xs-12 count">
                           <div
                              data-animation-origin="top"
                              data-animation-duration="300"
                              data-animation-delay="600"
                              data-animation-distance="35px">
                              <div className="count-icon">
                                 <i className="fa fa-windows"></i>
                              </div>
                              <span className=".integers digit font-accident-two-normal windows" data-counter="0">0</span>

                              <div className="count-text font-accident-two-bold">Windows APP</div>
                           </div>
                        </div>
                        <div className="col-lg-3 col-sm-6 col-xs-12 count">
                           <div
                              data-animation-origin="top"
                              data-animation-duration="300"
                              data-animation-delay="800"
                              data-animation-distance="35px">
                              <div className="count-icon">
                                 <i className="fa fa-bars"></i>
                              </div>
                              <span className=".integers digit font-accident-two-normal all" data-counter="0">0</span>

                              <div className="count-text font-accident-two-bold">All</div>
                           </div>
                        </div>
                     </div>
                     <div className="dividewhite2"></div>
                  </div>
               </section>
               <section id="portfolio-block" className="inner-section" data-section="portfolio">
                  <div className="dividewhite6"></div>
                  <div id="isotope-filters" className="port-filter port-filter-light text-center"
                     data-animation-origin="top"
                     data-animation-duration="500"
                     data-animation-delay="200"
                     data-animation-distance="25px">
                     <ul>
                        <li><a href="#all" data-filter="*">All</a></li>
                        <li><a href="#web" data-filter=".web">Web</a></li>
                        <li><a href="#mobile" data-filter=".mobile">Mobile</a></li>
                        <li><a href="#windows" data-filter=".windows">Windows</a></li>
                     </ul>
                  </div>
                  <div className="dividewhite6"></div>
                  <div className="row masonry-row">
                     <div className="grid container-fluid text-center">
                        <div id="posts" className="row popup-container">
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="left"
                                    data-animation-duration="600"
                                    data-animation-delay="400"
                                    data-animation-distance="100px">
                                    <div className="popup-call gallery-group-item" data-group="car">
                                       <a href="./assets/images/car/1.jpg" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/car/2.jpg" className="gallery-item" title=""></a>
                                       <a href="./assets/images/car/3.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/car/4.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/car/5.jpg" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/car/logo.png" className="img-responsive'" alt="car" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Arvand Cars Management</h3>
                                       </div>
                                       <a data-content="protfolio/car" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer mobile col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="carapp">
                                       <a href="./assets/images/carapp/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/carapp/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/5.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/6.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/7.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/8.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/9.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/10.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/11.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/12.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/13.png" className="gallery-item"></a>
                                       <a href="./assets/images/carapp/14.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/carapp/logo.jpg" className="img-responsive" alt="carapp" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Arvand Cars Management App</h3>
                                       </div>
                                       <a data-content="protfolio/carapp" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer web seo col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="top"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="work">
                                       <a href="./assets/images/work/1.jpg" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/work/2.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/work/3.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/work/4.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/work/5.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/work/6.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/work/7.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/work/8.jpg" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/work/logo.jpg" className="img-responsive" alt="work" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Arvand Customs Management</h3>
                                       </div>
                                       <a data-content="protfolio/work" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="gis">
                                       <a href="./assets/images/gis/1.jpg" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/gis/2.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/gis/3.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/gis/4.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/gis/5.jpg" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/gis/logo.jpg" className="img-responsive" alt="gis" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>GIS Map Management</h3>
                                       </div>
                                       <a data-content="protfolio/gis" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer mobile col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="vocabulary">
                                       <a href="./assets/images/vocabulary/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/vocabulary/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/5.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/6.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/7.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/8.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/9.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/10.png" className="gallery-item"></a>
                                       <a href="./assets/images/vocabulary/11.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/vocabulary/logo.jpg" className="img-responsive" alt="vocabulary" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Vocabulary App</h3>
                                       </div>
                                       <a data-content="protfolio/vocabulary" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer mobile col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="arvandyar">
                                       <a href="./assets/images/arvandyar/1.jpg" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/arvandyar/2.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/arvandyar/3.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/arvandyar/4.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/arvandyar/5.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/arvandyar/6.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/arvandyar/7.jpg" className="gallery-item"></a>
                                       <a href="./assets/images/arvandyar/8.jpg" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/arvandyar/logo.png" className="img-responsive" alt="arvandyar" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Arvand Yar App</h3>
                                       </div>
                                       <a data-content="protfolio/arvandyar" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="shandiz">
                                       <a href="./assets/images/shandiz/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/shandiz/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/shandiz/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/shandiz/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/shandiz/5.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/shandiz/logo.jpg" className="img-responsive" alt="shandiz" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Shandiz Web shop</h3>
                                       </div>
                                       <a data-content="protfolio/shandiz" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="company">
                                       <a href="./assets/images/company/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/company/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/5.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/6.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/7.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/8.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/9.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/10.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/11.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/12.png" className="gallery-item"></a>
                                       <a href="./assets/images/company/13.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/company/logo.jpg" className="img-responsive" alt="company" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Company Registration</h3>
                                       </div>
                                       <a data-content="protfolio/company" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer windows col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="goranTransport">
                                       <a href="./assets/images/goranTransport/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/goranTransport/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/5.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/6.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/7.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/8.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/9.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/10.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/11.png" className="gallery-item"></a>
                                       <a href="./assets/images/goranTransport/12.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/goranTransport/logo.jpg" className="img-responsive" alt="goranTransport" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Goran Transport</h3>
                                       </div>
                                       <a data-content="protfolio/goranTransport" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer windows col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="solutionIt">
                                       <a href="./assets/images/solutionIt/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/solutionIt/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/5.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/6.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/7.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/8.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/9.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/10.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/11.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/12.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/13.png" className="gallery-item"></a>
                                       <a href="./assets/images/solutionIt/14.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/solutionIt/logo.png" className="img-responsive" alt="solutionIt" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Solutions IT Dr</h3>
                                       </div>
                                       <a data-content="protfolio/solutionIt" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="food">
                                       <a href="./assets/images/food/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/food/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/food/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/food/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/food/5.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/food/logo.jpg" className="img-responsive" alt="food" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Food Reservation</h3>
                                       </div>
                                       <a data-content="protfolio/food" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           {/*  <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="attendanceDevice">
                                       <a href="./assets/images/attendanceDevice/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/attendanceDevice/14.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/attendanceDevice/logo.jpg" className="img-responsive" alt="attendanceDevice" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Attendance Device</h3>
                                          <p>#CSharp</p>
                                       </div>
                                       <a href="#attendanceDevice"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div> */}
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="cineport">
                                       <a href="./assets/images/cineport/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/cineport/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/cineport/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/cineport/4.png" className="gallery-item"></a>
                                       <a href="./assets/images/cineport/5.png" className="gallery-item"></a>
                                       <a href="./assets/images/cineport/6.png" className="gallery-item"></a>
                                       <a href="./assets/images/cineport/7.png" className="gallery-item"></a>
                                       <a href="./assets/images/cineport/8.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/cineport/logo.jpg" className="img-responsive" alt="cineport" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Cineport</h3>
                                       </div>
                                       <a data-content="protfolio/cineport" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="adic">
                                       <a href="./assets/images/adic/1.png" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/adic/2.png" className="gallery-item"></a>
                                       <a href="./assets/images/adic/3.png" className="gallery-item"></a>
                                       <a href="./assets/images/adic/4.png" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/adic/logo.jpg" className="img-responsive" alt="adic" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Adic</h3>
                                       </div>
                                       <a data-content="protfolio/adic" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                           <div className="grid-item grid-sizer windows col-xs-6 col-sm-4 col-md-3">
                              <div className="item-wrap">
                                 <figure className="effect-goliath ui-menu-color02"
                                    data-animation-origin="right"
                                    data-animation-duration="400"
                                    data-animation-delay="400"
                                    data-animation-distance="50px">
                                    <div className="popup-call gallery-group-item" data-group="regioExpress">
                                       <a href="./assets/images/regioExpress/1.jpeg" className="gallery-item"><i className="flaticon-tool"></i></a>
                                       <a href="./assets/images/regioExpress/2.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/3.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/4.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/5.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/6.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/7.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/8.jpeg" className="gallery-item"></a>
                                       <a href="./assets/images/regioExpress/9.jpeg" className="gallery-item"></a>
                                    </div>
                                    <img src="./assets/images/regioExpress/logo.jpg" className="img-responsive" alt="regioExpress" />
                                    <figcaption>
                                       <div className="fig-description">
                                          <h3>Regio Express</h3>
                                       </div>
                                       <a data-content="protfolio/regioExpress" href="#"></a>
                                    </figcaption>
                                 </figure>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div className="dividewhite8"></div>
               </section>
            </div>
         </div>
      );
   }
}
let domContainer = document.querySelector('#containerBody');
ReactDOM.render(<ProtfolioContent />, domContainer);