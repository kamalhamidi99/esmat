'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProtfolioShandizContent = function (_React$Component) {
    _inherits(ProtfolioShandizContent, _React$Component);

    function ProtfolioShandizContent(props) {
        _classCallCheck(this, ProtfolioShandizContent);

        return _possibleConstructorReturn(this, (ProtfolioShandizContent.__proto__ || Object.getPrototypeOf(ProtfolioShandizContent)).call(this, props));
    }

    _createClass(ProtfolioShandizContent, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "content-wrap" },
                React.createElement(
                    "div",
                    { id: "portfolio-item-page", className: "inner-content" },
                    React.createElement(
                        "section",
                        { className: "inner-section" },
                        React.createElement(
                            "div",
                            { className: "post-header",
                                "data-animation-origin": "right",
                                "data-animation-duration": "400",
                                "data-animation-delay": "100",
                                "data-animation-distance": "50px" },
                            React.createElement(
                                "h2",
                                { className: "font-accident-two-normal" },
                                "Shandiz Web shop"
                            ),
                            React.createElement("p", null),
                            React.createElement("div", { className: "dividewhite1" })
                        ),
                        React.createElement("div", { className: "dividewhite4" }),
                        React.createElement(
                            "div",
                            { className: "row" },
                            React.createElement(
                                "div",
                                { className: "col-md-4",
                                    "data-animation-origin": "top",
                                    "data-animation-duration": "400",
                                    "data-animation-delay": "400",
                                    "data-animation-distance": "50px" },
                                React.createElement(
                                    "div",
                                    { className: "row" },
                                    React.createElement(
                                        "div",
                                        { className: "col-md-12 col-sm-6 col-xs-12" },
                                        React.createElement("img", { src: "./assets/images/shandiz/1.png", className: "img-responsive radius-4", alt: "portfolio item" }),
                                        React.createElement("div", { className: "dividewhite2" })
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-md-12 col-sm-6 col-xs-12" },
                                        React.createElement(
                                            "div",
                                            { className: "popup-call gallery-group-item", "data-group": "car" },
                                            React.createElement(
                                                "a",
                                                { href: "./assets/images/shandiz/1.png", className: "gallery-item btn btn-default" },
                                                "View All"
                                            ),
                                            React.createElement("a", { href: "./assets/images/shandiz/2.png", className: "gallery-item" }),
                                            React.createElement("a", { href: "./assets/images/shandiz/3.png", className: "gallery-item" }),
                                            React.createElement("a", { href: "./assets/images/shandiz/4.png", className: "gallery-item" }),
                                            React.createElement("a", { href: "./assets/images/shandiz/5.png", className: "gallery-item" })
                                        )
                                    )
                                ),
                                React.createElement("div", { className: "dividewhite6" })
                            ),
                            React.createElement(
                                "div",
                                { id: "portfolio-overview", className: "col-md-8",
                                    "data-animation-origin": "top",
                                    "data-animation-duration": "400",
                                    "data-animation-delay": "600",
                                    "data-animation-distance": "50px" },
                                React.createElement(
                                    "h3",
                                    { className: "font-accident-two-light" },
                                    "Project Data"
                                ),
                                React.createElement("div", { className: "dividewhite4" }),
                                React.createElement(
                                    "div",
                                    { className: "portfolio-item-details table" },
                                    React.createElement(
                                        "div",
                                        { className: "row table-row" },
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            "Categories:"
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            React.createElement(
                                                "span",
                                                null,
                                                "Web"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row table-row" },
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            "Author:"
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            React.createElement(
                                                "span",
                                                null,
                                                "Esmat Zilaei"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row table-row" },
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            "Client:"
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            React.createElement(
                                                "span",
                                                null,
                                                "Solutions-It Drenthe"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row table-row" },
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            "Url:"
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "table-cell" },
                                            React.createElement(
                                                "span",
                                                null,
                                                React.createElement(
                                                    "a",
                                                    { href: "https://shandiz.nl/" },
                                                    "https://shandiz.nl"
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement("div", { className: "dividewhite4" }),
                                React.createElement(
                                    "p",
                                    null,
                                    "This website is designed to sell a variety of small and large food items."
                                ),
                                React.createElement("div", { className: "dividewhite4" }),
                                React.createElement(
                                    "div",
                                    { className: "row" },
                                    React.createElement(
                                        "div",
                                        { className: "col-md-4" },
                                        React.createElement(
                                            "h3",
                                            { className: "font-accident-two-light" },
                                            "Technology"
                                        ),
                                        React.createElement("div", { className: "dividewhite2" }),
                                        React.createElement(
                                            "ul",
                                            { className: "portfolio-item-details" },
                                            React.createElement(
                                                "li",
                                                null,
                                                "Asp.net Mvc"
                                            ),
                                            React.createElement(
                                                "li",
                                                null,
                                                "C#"
                                            ),
                                            React.createElement(
                                                "li",
                                                null,
                                                "Sql Server"
                                            ),
                                            React.createElement(
                                                "li",
                                                null,
                                                "Bootstrap"
                                            ),
                                            React.createElement(
                                                "li",
                                                null,
                                                "jQuery"
                                            )
                                        ),
                                        React.createElement("div", { className: "dividewhite4" })
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-md-8" },
                                        React.createElement(
                                            "h3",
                                            { className: "font-accident-two-light" },
                                            "Share"
                                        ),
                                        React.createElement("div", { className: "dividewhite2" }),
                                        React.createElement(
                                            "div",
                                            { className: "share" },
                                            React.createElement(
                                                "ul",
                                                { className: "list-inline social" },
                                                React.createElement(
                                                    "li",
                                                    null,
                                                    React.createElement(
                                                        "a",
                                                        { target: "_blank", href: "#", className: "rst-icon-facebook" },
                                                        React.createElement("i", { className: "fa fa-facebook" })
                                                    )
                                                ),
                                                React.createElement(
                                                    "li",
                                                    null,
                                                    React.createElement(
                                                        "a",
                                                        { target: "_blank", href: "#", className: "rst-icon-twitter" },
                                                        React.createElement("i", { className: "fa fa-twitter" })
                                                    )
                                                ),
                                                React.createElement(
                                                    "li",
                                                    null,
                                                    React.createElement(
                                                        "a",
                                                        { target: "_blank", href: "#", className: "rst-icon-pinterest" },
                                                        React.createElement("i", { className: "fa fa-pinterest" })
                                                    )
                                                ),
                                                React.createElement(
                                                    "li",
                                                    null,
                                                    React.createElement(
                                                        "a",
                                                        { target: "_blank", href: "#", className: "rst-icon-instagram" },
                                                        React.createElement("i", { className: "fa fa-instagram" })
                                                    )
                                                ),
                                                React.createElement(
                                                    "li",
                                                    null,
                                                    React.createElement(
                                                        "a",
                                                        { target: "_blank", href: "#", className: "rst-icon-google" },
                                                        React.createElement("i", { className: "fa fa-google-plus" })
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        React.createElement("div", { className: "dividewhite8" })
                    )
                )
            );
        }
    }]);

    return ProtfolioShandizContent;
}(React.Component);

var domContainer = document.querySelector('#containerBody');
ReactDOM.render(React.createElement(ProtfolioShandizContent, null), domContainer);