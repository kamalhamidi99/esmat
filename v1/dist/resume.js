'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ResumeContent = function (_React$Component) {
	_inherits(ResumeContent, _React$Component);

	function ResumeContent(props) {
		_classCallCheck(this, ResumeContent);

		return _possibleConstructorReturn(this, (ResumeContent.__proto__ || Object.getPrototypeOf(ResumeContent)).call(this, props));
	}

	_createClass(ResumeContent, [{
		key: "render",
		value: function render() {
			return React.createElement(
				"div",
				{ className: "content-wrap" },
				React.createElement(
					"div",
					{ id: "resume", className: "inner-content" },
					React.createElement(
						"section",
						{ id: "page-title", className: "inner-section ui-menu-color02" },
						React.createElement(
							"div",
							{ className: "container-fluid nopadding" },
							React.createElement(
								"h2",
								{ className: "font-accident-two-light color01 uppercase",
									"data-animation-origin": "right",
									"data-animation-duration": "400",
									"data-animation-delay": "100",
									"data-animation-distance": "50px" },
								"Resume"
							)
						)
					),
					React.createElement(
						"section",
						{ className: "inner-section light bg-color01" },
						React.createElement(
							"div",
							{ className: "container-fluid nopadding" },
							React.createElement(
								"div",
								{ "data-animation-origin": "top",
									"data-animation-duration": "200",
									"data-animation-delay": "300",
									"data-animation-distance": "20px" },
								React.createElement(
									"h3",
									{ className: "font-accident-two-normal uppercase text-center" },
									"Facts About Me"
								),
								React.createElement("div", { className: "dividewhite1" }),
								React.createElement(
									"p",
									{ className: "small text-center fontcolor-medium" },
									"Here are some things that you should know about me:"
								),
								React.createElement("div", { className: "dividewhite4" })
							),
							React.createElement(
								"div",
								{ className: "row" },
								React.createElement(
									"div",
									{ className: "col-md-6",
										"data-animation-origin": "top",
										"data-animation-duration": "400",
										"data-animation-delay": "600",
										"data-animation-distance": "30px" },
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Status:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Open to Offers"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Desired Salary:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"To be Discussed"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Education Level:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Bachelor\u2019s Degree, Computer Software Engineering"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Job Type:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Contract, Freelance"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Mobility:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Ready to Relocate"
											)
										)
									)
								),
								React.createElement(
									"div",
									{ className: "col-md-6",
										"data-animation-origin": "top",
										"data-animation-duration": "400",
										"data-animation-delay": "800",
										"data-animation-distance": "30px" },
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Languages:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"English, Persian"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Position:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Senior Programmer"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Industry:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Web Development"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Speciality:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Web, Mobile and Windows Development and Design"
											)
										)
									),
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-sm-4" },
											React.createElement(
												"span",
												{ className: "font-accident-two-bold uppercase" },
												"Hobbies:"
											)
										),
										React.createElement(
											"div",
											{ className: "col-sm-8" },
											React.createElement(
												"p",
												{ className: "" },
												"Books, Movies, Blogs"
											)
										)
									)
								)
							)
						)
					),
					React.createElement(
						"section",
						{ id: "m-details", className: "inner-section light bg-color02" },
						React.createElement(
							"div",
							{ className: "container-fluid nopadding" },
							React.createElement(
								"div",
								{ "data-animation-origin": "top",
									"data-animation-duration": "100",
									"data-animation-delay": "300",
									"data-animation-distance": "30px" },
								React.createElement(
									"h3",
									{ className: "font-accident-two-normal uppercase text-center" },
									"Personal qualities"
								),
								React.createElement("div", { className: "dividewhite1" }),
								React.createElement("div", { className: "dividewhite6" })
							),
							React.createElement(
								"div",
								{ className: "row" },
								React.createElement(
									"div",
									{ className: "col-md-3 infoblock text-center",
										"data-animation-origin": "left",
										"data-animation-duration": "400",
										"data-animation-delay": "400",
										"data-animation-distance": "50px" },
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("i", { className: "flaticon-photo246" })
										),
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("div", { className: "dividewhite1" }),
											React.createElement(
												"h5",
												{ className: "font-accident-two-bold uppercase" },
												"Clean Code"
											),
											React.createElement(
												"p",
												{ className: "small" },
												"Write clean, well-organized, well-documented code that meets company standards."
											)
										)
									),
									React.createElement("div", { className: "divider-dynamic" })
								),
								React.createElement(
									"div",
									{ className: "col-md-3 infoblock text-center",
										"data-animation-origin": "left",
										"data-animation-duration": "400",
										"data-animation-delay": "200",
										"data-animation-distance": "25px" },
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("i", { className: "flaticon-pie-graph" })
										),
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("div", { className: "dividewhite1" }),
											React.createElement(
												"h5",
												{ className: "font-accident-two-bold uppercase" },
												"Optimized"
											),
											React.createElement(
												"p",
												{ className: "small" },
												"Ability to create mobile optimized and user-friendly website designs, ensuring ease of navigation, well organized content."
											)
										)
									),
									React.createElement("div", { className: "divider-dynamic" })
								),
								React.createElement(
									"div",
									{ className: "col-md-3 infoblock text-center",
										"data-animation-origin": "right",
										"data-animation-duration": "400",
										"data-animation-delay": "200",
										"data-animation-distance": "25px" },
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("i", { className: "flaticon-clocks18" })
										),
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("div", { className: "dividewhite1" }),
											React.createElement(
												"h5",
												{ className: "font-accident-two-bold uppercase" },
												"Customizable"
											),
											React.createElement(
												"p",
												{ className: "small" },
												"The Ready-to-use pre-defined color css classNamees make possible to Customize your websire in a wide range."
											)
										)
									),
									React.createElement("div", { className: "divider-dynamic" })
								),
								React.createElement(
									"div",
									{ className: "col-md-3 infoblock text-center",
										"data-animation-origin": "right",
										"data-animation-duration": "400",
										"data-animation-delay": "400",
										"data-animation-distance": "50px" },
									React.createElement(
										"div",
										{ className: "row" },
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("i", { className: "flaticon-stats47" })
										),
										React.createElement(
											"div",
											{ className: "col-md-12" },
											React.createElement("div", { className: "dividewhite1" }),
											React.createElement(
												"h5",
												{ className: "font-accident-two-bold uppercase" },
												"Experience"
											),
											React.createElement(
												"p",
												{ className: "small" },
												"Over 6 years of experience in the fields of Web and desktop programming"
											)
										)
									),
									React.createElement("div", { className: "divider-dynamic" })
								)
							)
						)
					),
					React.createElement(
						"section",
						{ id: "timeline", className: "light inner-section" },
						React.createElement(
							"div",
							{ className: "container-fluid nopadding" },
							React.createElement(
								"div",
								{ className: "text-center",
									"data-animation-origin": "top",
									"data-animation-duration": "400",
									"data-animation-delay": "400",
									"data-animation-distance": "30px" },
								React.createElement("div", { className: "dividewhite4" }),
								React.createElement(
									"h3",
									{ className: "font-accident-two-normal uppercase" },
									"Timeline"
								),
								React.createElement(
									"h5",
									{ className: "font-accident-two-normal uppercase subtitle" },
									"My Work Experience and Education"
								),
								React.createElement("div", { className: "dividewhite1" }),
								React.createElement(
									"p",
									{ className: "small fontcolor-medium" },
									"I graduated in computer science in 2013 and started working after that. Here is my timeline so far."
								)
							),
							React.createElement("div", { className: "dividewhite4" }),
							React.createElement(
								"ul",
								{ className: "timeline-vert timeline" },
								React.createElement(
									"li",
									{ className: "timeline-inverted" },
									React.createElement(
										"div",
										{ className: "timeline-badge primary" },
										React.createElement("i", { className: "flaticon-clocks18" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "left",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2015 - Persent"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Programmer"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"at Solutions-it Drenthe (Netherlands)"
											),
											React.createElement(
												"p",
												null,
												"In 2015, I met Mr. Hossen Bazian, who is the CEO of Solutions-it Drenthe in the Netherlands. He suggested to me to work for him as telecommunications and project-based contracts."
											)
										)
									)
								),
								React.createElement(
									"li",
									null,
									React.createElement(
										"div",
										{ className: "timeline-badge primary" },
										React.createElement("i", { className: "flaticon-clocks18" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "left",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2015"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Certification"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"Secure Coding in .NET: Developing Defensible Applications"
											),
											React.createElement(
												"p",
												null,
												"In 2015, I went to private school to learn more about the security of web applications and got my second Certification in there."
											)
										)
									)
								),
								React.createElement(
									"li",
									{ className: "timeline-inverted" },
									React.createElement(
										"div",
										{ className: "timeline-badge primary" },
										React.createElement("i", { className: "flaticon-clocks18" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "left",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2014 - Persent"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Senior Programmer"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"at Tejarat Pardazan Electronic Company"
											),
											React.createElement(
												"p",
												null,
												"In 2014, I changed my workplace and got haired in Tejarat Pardazan Electronic. In there I learned a lot of new stuff and quickly grew fast."
											)
										)
									)
								),
								React.createElement(
									"li",
									null,
									React.createElement(
										"div",
										{ className: "timeline-badge success" },
										React.createElement("i", { className: "flaticon-clocks18" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "right",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2013 - 2014"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Programmer"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"at Pasha Sanat Yaran Company"
											),
											React.createElement(
												"p",
												null,
												"In 2013, After I finished my study, I started to word at Pasha Sanat Yaran Company as a front-end and backend."
											)
										)
									)
								),
								React.createElement(
									"li",
									{ className: "timeline-inverted" },
									React.createElement(
										"div",
										{ className: "timeline-badge success" },
										React.createElement("i", { className: "flaticon-clocks18" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "right",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2013"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Certifications"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"Engineering in enterprise web development with asp.net"
											),
											React.createElement(
												"p",
												null,
												"In 2013, I felt that I needed more training in web development, so I took classNamees in the `Technical & Vocational Training Organization` and got my first certificate in `Engineering in enterprise web development with asp.net` with a score of 88/100."
											)
										)
									)
								),
								React.createElement(
									"li",
									null,
									React.createElement(
										"div",
										{ className: "timeline-badge danger" },
										React.createElement("i", { className: "flaticon-graduation61" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "left",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2010 - 2013"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Bachelor\u2019s Degree"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"Computer Software Engineering"
											),
											React.createElement(
												"p",
												null,
												"In 2010, After associate's degree, I took the entrance exam and entered the faculty of Karun Higher Education Institute for my bachelor degree."
											)
										)
									)
								),
								React.createElement(
									"li",
									{ className: "timeline-inverted info" },
									React.createElement(
										"div",
										{ className: "timeline-badge warning" },
										React.createElement("i", { className: "flaticon-graduation61" })
									),
									React.createElement(
										"div",
										{ className: "timeline-panel",
											"data-animation-origin": "right",
											"data-animation-duration": "400",
											"data-animation-delay": "400",
											"data-animation-distance": "25px" },
										React.createElement(
											"p",
											{ className: "timeline-time fontcolor-invert" },
											"2008 - 2010"
										),
										React.createElement("div", { className: "timeline-photo" }),
										React.createElement(
											"div",
											{ className: "timeline-heading" },
											React.createElement(
												"h4",
												{ className: "font-accident-two-normal uppercase" },
												"Associate's Degree"
											),
											React.createElement(
												"h6",
												{ className: "uppercase" },
												"Computer Science"
											),
											React.createElement(
												"p",
												null,
												"In 2010, I got my associate's degree from Islamic Azad University."
											)
										)
									)
								)
							),
							React.createElement(
								"div",
								{ className: "text-center" },
								React.createElement(
									"a",
									{ className: "btn btn-darker" },
									"Start"
								)
							),
							React.createElement("div", { className: "dividewhite6" })
						)
					)
				)
			);
		}
	}]);

	return ResumeContent;
}(React.Component);

var domContainer = document.querySelector('#containerBody');
ReactDOM.render(React.createElement(ResumeContent, null), domContainer);