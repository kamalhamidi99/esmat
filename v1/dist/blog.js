'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BlogContent = function (_React$Component) {
    _inherits(BlogContent, _React$Component);

    function BlogContent(props) {
        _classCallCheck(this, BlogContent);

        return _possibleConstructorReturn(this, (BlogContent.__proto__ || Object.getPrototypeOf(BlogContent)).call(this, props));
    }

    _createClass(BlogContent, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "content-wrap" },
                React.createElement(
                    "div",
                    { id: "blog", className: "inner-content", style: { minHeight: 'auto' } },
                    React.createElement(
                        "section",
                        { id: "page-title", className: "inner-section ui-menu-color06" },
                        React.createElement(
                            "div",
                            { className: "container-fluid nopadding" },
                            React.createElement(
                                "h2",
                                { className: "font-accident-two-light color01 uppercase",
                                    "data-animation-origin": "right",
                                    "data-animation-duration": "400",
                                    "data-animation-delay": "100",
                                    "data-animation-distance": "50px" },
                                "Blog"
                            ),
                            React.createElement(
                                "h4",
                                { className: "font-accident-two-normal color01 uppercase subtitle",
                                    "data-animation-origin": "right",
                                    "data-animation-duration": "400",
                                    "data-animation-delay": "200",
                                    "data-animation-distance": "50px" },
                                "Coming Soon"
                            )
                        )
                    )
                )
            );
        }
    }]);

    return BlogContent;
}(React.Component);

var domContainer = document.querySelector('#containerBody');
ReactDOM.render(React.createElement(BlogContent, null), domContainer);