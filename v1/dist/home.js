'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HomeContent = function (_React$Component) {
    _inherits(HomeContent, _React$Component);

    function HomeContent(props) {
        _classCallCheck(this, HomeContent);

        return _possibleConstructorReturn(this, (HomeContent.__proto__ || Object.getPrototypeOf(HomeContent)).call(this, props));
    }

    _createClass(HomeContent, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "content-wrap" },
                React.createElement(
                    "section",
                    { className: "container-fluid", "data-section": "home" },
                    React.createElement(
                        "div",
                        { className: "row flex-row" },
                        React.createElement(
                            "div",
                            { id: "personal-2", className: "col-md-4 flex-column ui-block-color01 light nopadding",
                                "data-animation-origin": "right", "data-animation-duration": "300", "data-animation-delay": "600",
                                "data-animation-distance": "200px" },
                            React.createElement(
                                "div",
                                { className: "row flex-panel nopadding" },
                                React.createElement(
                                    "div",
                                    { className: "col-md-12 nopadding" },
                                    React.createElement(
                                        "div",
                                        { className: "row flex-row nopadding" },
                                        React.createElement(
                                            "span",
                                            { className: "col-md-6 flex-column bg-color02 p-grid-item nopadding hvr-bounce-to-top hvr-green" },
                                            React.createElement(
                                                "div",
                                                { className: "flex-panel padding-44" },
                                                React.createElement("div", { className: "p-icon green" }),
                                                React.createElement(
                                                    "span",
                                                    { className: "uppercase" },
                                                    "Status"
                                                ),
                                                React.createElement(
                                                    "p",
                                                    { className: "" },
                                                    "Open to offers"
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            "span",
                                            { className: "col-md-6 flex-column bg-color03 p-grid-item nopadding hvr-bounce-to-top hvr-blue" },
                                            React.createElement(
                                                "div",
                                                { className: "flex-panel padding-44" },
                                                React.createElement("div", { className: "p-icon blue" }),
                                                React.createElement(
                                                    "span",
                                                    { className: "uppercase" },
                                                    "Desired Salary"
                                                ),
                                                React.createElement(
                                                    "p",
                                                    { className: "" },
                                                    "To be Discussed"
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "col-md-12 nopadding" },
                                    React.createElement(
                                        "div",
                                        { className: "row flex-row nopadding" },
                                        React.createElement(
                                            "span",
                                            { className: "col-md-6 flex-column bg-color03 p-grid-item nopadding hvr-bounce-to-top hvr-red" },
                                            React.createElement(
                                                "div",
                                                { className: "flex-panel padding-44" },
                                                React.createElement("div", { className: "p-icon red" }),
                                                React.createElement(
                                                    "span",
                                                    { className: "uppercase" },
                                                    "Job Type"
                                                ),
                                                React.createElement(
                                                    "p",
                                                    { className: "" },
                                                    "Contract, Freelance"
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            "span",
                                            { className: "col-md-6 flex-column bg-color02 p-grid-item nopadding hvr-bounce-to-top hvr-yellow" },
                                            React.createElement(
                                                "div",
                                                { className: "flex-panel padding-44" },
                                                React.createElement("div", { className: "p-icon yellow" }),
                                                React.createElement(
                                                    "span",
                                                    { className: "uppercase" },
                                                    "Mobility"
                                                ),
                                                React.createElement(
                                                    "p",
                                                    { className: "" },
                                                    "Ready to Relocate"
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "col-md-12 nopadding" },
                                    React.createElement(
                                        "div",
                                        { className: "row flex-row nopadding" },
                                        React.createElement(
                                            "span",
                                            { className: "col-md-6 flex-column bg-color02 p-grid-item nopadding hvr-bounce-to-top hvr-blue" },
                                            React.createElement(
                                                "div",
                                                { className: "flex-panel padding-44" },
                                                React.createElement("div", { className: "p-icon blue" }),
                                                React.createElement(
                                                    "span",
                                                    { className: "uppercase" },
                                                    "Position"
                                                ),
                                                React.createElement(
                                                    "p",
                                                    { className: "" },
                                                    "Senior Programmer"
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            "span",
                                            { className: "col-md-6 flex-column bg-color03 p-grid-item nopadding hvr-bounce-to-top hvr-orange" },
                                            React.createElement(
                                                "div",
                                                { className: "flex-panel padding-44" },
                                                React.createElement("div", { className: "p-icon orange" }),
                                                React.createElement(
                                                    "span",
                                                    { className: "uppercase" },
                                                    "Industry"
                                                ),
                                                React.createElement(
                                                    "p",
                                                    { className: "" },
                                                    "Web Development"
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        React.createElement(
                            "div",
                            { id: "details", className: "col-md-8 flex-column bg-color01 light nopadding", "data-animation-origin": "left",
                                "data-animation-duration": "300", "data-animation-delay": "400", "data-animation-distance": "200px" },
                            React.createElement(
                                "div",
                                { className: "padding-50 flex-panel" },
                                React.createElement(
                                    "div",
                                    { className: "row row-no-padding" },
                                    React.createElement(
                                        "div",
                                        { className: "col-md-12 nopadding" },
                                        React.createElement(
                                            "h3",
                                            { className: "font-accident-two-normal uppercase" },
                                            "A little thing about me"
                                        ),
                                        React.createElement("div", { className: "quote" })
                                    )
                                ),
                                React.createElement("div", { className: "divider-dynamic" }),
                                React.createElement(
                                    "div",
                                    { className: "row nopadding" },
                                    React.createElement(
                                        "div",
                                        { className: "col-md-12 infoblock nopadding" },
                                        React.createElement(
                                            "span",
                                            null,
                                            "Hello, my name is Esmat. I'm 31 years old. I have 6 years of professional experience. I work as a web developer, also along the way, I learned to build windows and mobile applications."
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                React.createElement(
                    "section",
                    { id: "professional", className: "container-fluid", "data-section": "home" },
                    React.createElement(
                        "div",
                        { className: "row flex-row" },
                        React.createElement(
                            "div",
                            { id: "pro-experience", className: "col-md-4 flex-column dark nopadding ui-block-color02 flex-col",
                                "data-animation-origin": "bottom", "data-animation-duration": "300", "data-animation-delay": "800",
                                "data-animation-distance": "200px" },
                            React.createElement(
                                "div",
                                { className: "padding-50 flex-panel" },
                                React.createElement(
                                    "h3",
                                    { className: "font-accident-two-normal uppercase" },
                                    "Employment"
                                ),
                                React.createElement("div", { className: "dividewhite4" }),
                                React.createElement(
                                    "div",
                                    { className: "experience" },
                                    React.createElement(
                                        "ul",
                                        { className: "" },
                                        React.createElement(
                                            "li",
                                            { className: "date" },
                                            "2016 - Present"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "company uppercase" },
                                            React.createElement(
                                                "a",
                                                { href: "https://www.solutions-it.nl/", target: "_blank" },
                                                "Solutions-it Drenthe"
                                            ),
                                            React.createElement(
                                                "small",
                                                { style: { fontSize: '10px' } },
                                                " (Netherlands) "
                                            )
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position" },
                                            "Programmer"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position", style: { marginTop: '4px', fontStyle: 'italic', fontSize: '12px' } },
                                            "Windows App & Web Developer"
                                        )
                                    ),
                                    React.createElement(
                                        "ul",
                                        { className: "" },
                                        React.createElement(
                                            "li",
                                            { className: "date" },
                                            "2014 - Present"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "company uppercase" },
                                            React.createElement(
                                                "a",
                                                { href: "http://tpe.co.ir/tejarat-pardazan-electronic/", target: "_blank" },
                                                "Tejarat Pardazan Electronic"
                                            ),
                                            React.createElement(
                                                "small",
                                                { style: { fontSize: '10px' } },
                                                " (Iran) "
                                            )
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position" },
                                            "Senior Programmer"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position", style: { marginTop: '4px', fontStyle: 'italic', fontSize: '12px' } },
                                            "Web Developer & Mobile Developer"
                                        )
                                    ),
                                    React.createElement(
                                        "ul",
                                        { className: "" },
                                        React.createElement(
                                            "li",
                                            { className: "date" },
                                            "2013 - 2014"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "company uppercase" },
                                            React.createElement(
                                                "a",
                                                null,
                                                "Pasha Sanat Yaran Company"
                                            ),
                                            React.createElement(
                                                "small",
                                                { style: { fontSize: '10px' } },
                                                " (Iran) "
                                            )
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position" },
                                            "Programmer"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position", style: { marginTop: '4px', fontStyle: 'italic', fontSize: '12px' } },
                                            "Web Developer & Web Designer"
                                        )
                                    ),
                                    React.createElement(
                                        "ul",
                                        null,
                                        React.createElement(
                                            "li",
                                            { className: "date" },
                                            "2014 - Present"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "company uppercase" },
                                            React.createElement(
                                                "a",
                                                null,
                                                "Freelancer"
                                            )
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position" },
                                            "Developer"
                                        ),
                                        React.createElement(
                                            "li",
                                            { className: "position", style: { marginTop: '4px', fontStyle: 'italic', fontSize: '12px' } },
                                            "Web Developer & Mobile Developer"
                                        )
                                    )
                                ),
                                React.createElement(
                                    "a",
                                    { "data-content": "resume", className: "btn btn-wh-trans btn-xs gototop" },
                                    "Learn More"
                                ),
                                React.createElement("div", { className: "dividewhite1" })
                            )
                        ),
                        React.createElement(
                            "div",
                            { id: "bar-skills", className: "col-md-8 flex-column dark nopadding ui-block-color03 flex-col",
                                "data-section": "bar-skills", "data-animation-origin": "right", "data-animation-duration": "400",
                                "data-animation-delay": "1100", "data-animation-distance": "200px" },
                            React.createElement(
                                "div",
                                { className: "row" },
                                React.createElement(
                                    "div",
                                    { className: "col-sm-12" },
                                    React.createElement(
                                        "div",
                                        { className: "padding-50 flex-panel", style: { paddingBottom: '20px' } },
                                        React.createElement(
                                            "h3",
                                            { className: "font-accident-two-normal uppercase" },
                                            "Professional skills"
                                        ),
                                        React.createElement("div", { className: "dividewhite2" }),
                                        React.createElement(
                                            "div",
                                            { className: "row" },
                                            React.createElement(
                                                "div",
                                                { className: "col-md-12" },
                                                React.createElement(
                                                    "h5",
                                                    { className: "font-accident-two-medium uppercase letter-spacing-03" },
                                                    "Web Development"
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "progress" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "progress-bar progress-bar-success", role: "progressbar", "aria-valuenow": "90",
                                                            "aria-valuemin": "0", "aria-valuemax": "100" },
                                                        "90%"
                                                    )
                                                ),
                                                React.createElement(
                                                    "h5",
                                                    { className: "font-accident-two-medium uppercase letter-spacing-03" },
                                                    "Mobile Development"
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "progress" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "progress-bar progress-bar-warning", role: "progressbar", "aria-valuenow": "70",
                                                            "aria-valuemin": "0", "aria-valuemax": "100" },
                                                        "70%"
                                                    )
                                                ),
                                                React.createElement(
                                                    "h5",
                                                    { className: "font-accident-two-medium uppercase letter-spacing-03" },
                                                    "Windows Development"
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "progress" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "progress-bar progress-bar-info", role: "progressbar", "aria-valuenow": "80",
                                                            "aria-valuemin": "0", "aria-valuemax": "100" },
                                                        "80%"
                                                    )
                                                ),
                                                React.createElement(
                                                    "h5",
                                                    { className: "font-accident-two-medium uppercase letter-spacing-03" },
                                                    "Designer"
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "progress" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "progress-bar progress-bar-danger", role: "progressbar", "aria-valuenow": "75",
                                                            "aria-valuemin": "0", "aria-valuemax": "100" },
                                                        "75%"
                                                    )
                                                ),
                                                React.createElement("div", { className: "dividewhite1" }),
                                                React.createElement(
                                                    "div",
                                                    { id: "circle-skills", className: "flex-column dark nopadding ui-block-color03 flex-col",
                                                        "data-section": "progress", "data-animation-origin": "right", "data-animation-duration": "400",
                                                        "data-animation-delay": "1100", "data-animation-distance": "200px" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "" },
                                                        React.createElement(
                                                            "div",
                                                            { className: "row" },
                                                            React.createElement(
                                                                "div",
                                                                { className: "col-md-3 col-sm-3 col-xs-12 nopadding" },
                                                                React.createElement(
                                                                    "div",
                                                                    { className: "progressbar text-center" },
                                                                    React.createElement("div", { id: "circle1", "data-progressbar": "circle",
                                                                        "data-progressbar-color": "#fff", "data-progressbar-trailcolor": "#fff",
                                                                        "data-progressbar-to": "{\"color\": \"#51f2ec\", \"width\": 4}",
                                                                        "data-progressbar-from": "{\"color\": \"#3498db\", \"width\": 4}",
                                                                        "data-progressbar-value": "0.85" }),
                                                                    React.createElement(
                                                                        "h4",
                                                                        { className: "font-accident-two-bold uppercase" },
                                                                        "C#"
                                                                    )
                                                                )
                                                            ),
                                                            React.createElement(
                                                                "div",
                                                                { className: "col-md-3 col-sm-3 col-xs-12 nopadding" },
                                                                React.createElement(
                                                                    "div",
                                                                    { className: "progressbar text-center" },
                                                                    React.createElement("div", { id: "circle2", "data-progressbar": "circle",
                                                                        "data-progressbar-color": "#fff", "data-progressbar-trailcolor": "#fff",
                                                                        "data-progressbar-to": "{\"color\": \"#ffd200\", \"width\": 4}",
                                                                        "data-progressbar-from": "{\"color\": \"#3498db\", \"width\": 4}",
                                                                        "data-progressbar-value": "0.80" }),
                                                                    React.createElement(
                                                                        "h4",
                                                                        { className: "font-accident-two-bold uppercase" },
                                                                        "SQL"
                                                                    )
                                                                )
                                                            ),
                                                            React.createElement(
                                                                "div",
                                                                { className: "col-md-3 col-sm-3 col-xs-12 nopadding" },
                                                                React.createElement(
                                                                    "div",
                                                                    { className: "progressbar text-center" },
                                                                    React.createElement("div", { id: "circle3", "data-progressbar": "circle",
                                                                        "data-progressbar-color": "#fff", "data-progressbar-trailcolor": "#fff",
                                                                        "data-progressbar-to": "{\"color\": \"#F09C88\", \"width\": 4}",
                                                                        "data-progressbar-from": "{\"color\": \"#3498db\", \"width\": 4}",
                                                                        "data-progressbar-value": "0.75" }),
                                                                    React.createElement(
                                                                        "h4",
                                                                        { className: "font-accident-two-bold uppercase" },
                                                                        "Javascript"
                                                                    )
                                                                )
                                                            ),
                                                            React.createElement(
                                                                "div",
                                                                { className: "col-md-3 col-sm-3 col-xs-12 nopadding" },
                                                                React.createElement(
                                                                    "div",
                                                                    { className: "progressbar text-center" },
                                                                    React.createElement("div", { id: "circle4", "data-progressbar": "circle",
                                                                        "data-progressbar-color": "#fff", "data-progressbar-trailcolor": "#fff",
                                                                        "data-progressbar-to": "{\"color\": \"#5cb85c\", \"width\": 4}",
                                                                        "data-progressbar-from": "{\"color\": \"#3498db\", \"width\": 4}",
                                                                        "data-progressbar-value": "0.95" }),
                                                                    React.createElement(
                                                                        "h4",
                                                                        { className: "font-accident-two-bold uppercase" },
                                                                        "HTML/CSS"
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "col-sm-12" },
                                    React.createElement(
                                        "div",
                                        { id: "clients", className: "light padding-50", "data-animation-origin": "bottom", "data-animation-duration": "500", "data-animation-delay": "500", "data-animation-distance": "50px", style: { visibility: 'visible' } },
                                        React.createElement(
                                            "div",
                                            { className: "row row-no-padding" },
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Bootstrap" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/bootstrap.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Gitlab" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/gitlab.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Highcharts" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/highcharts.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "jQuery" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/jquery.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "LeafletJs" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/leaflet.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Adobe Photoshop" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/photoshop.jpg", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "ReactJs" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/reactjs.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Stimulsoft Reports" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/stimulsoft.jpg", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Xamarin Forms" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/xamarin.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Adobe Xd" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/xd.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Visual Studio Code" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/code.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Asp.net Mvc" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/mvc.png", className: "img-responsive" })
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "row row-no-padding", style: { marginTop: '15px' } },
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Asp.net Core" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/core.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Sql Server Management Studio" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/ssms.png", className: "img-responsive" })
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-1 col-sm-2 col-xs-4" },
                                                React.createElement(
                                                    "a",
                                                    { title: "Redis" },
                                                    React.createElement("img", { src: "../../assets/custom/2.2.2/images/layouts/all/redis.png", className: "img-responsive" })
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return HomeContent;
}(React.Component);

var domContainer = document.querySelector('#containerBody');
ReactDOM.render(React.createElement(HomeContent, null), domContainer);