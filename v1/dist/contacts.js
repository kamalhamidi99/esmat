'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ContactContent = function (_React$Component) {
    _inherits(ContactContent, _React$Component);

    function ContactContent(props) {
        _classCallCheck(this, ContactContent);

        return _possibleConstructorReturn(this, (ContactContent.__proto__ || Object.getPrototypeOf(ContactContent)).call(this, props));
    }

    _createClass(ContactContent, [{
        key: "render",
        value: function render() {
            return React.createElement(
                "div",
                { className: "content-wrap" },
                React.createElement(
                    "div",
                    { id: "contacts", className: "inner-content" },
                    React.createElement(
                        "section",
                        { id: "page-title", className: "inner-section ui-menu-color04" },
                        React.createElement(
                            "div",
                            { className: "container-fluid nopadding" },
                            React.createElement(
                                "h2",
                                { className: "font-accident-two-light color01 uppercase",
                                    "data-animation-origin": "right",
                                    "data-animation-duration": "400",
                                    "data-animation-delay": "100",
                                    "data-animation-distance": "50px" },
                                "Contacts"
                            )
                        )
                    ),
                    React.createElement(
                        "section",
                        { id: "contacts-data", className: "inner-block" },
                        React.createElement(
                            "div",
                            { className: "container-fluid nopadding" },
                            React.createElement("div", { className: "dividewhite5" }),
                            React.createElement(
                                "div",
                                { className: "row" },
                                React.createElement(
                                    "div",
                                    { className: "col-md-6",
                                        "data-animation-origin": "right",
                                        "data-animation-duration": "500",
                                        "data-animation-delay": "500",
                                        "data-animation-distance": "25px" },
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-12" },
                                            React.createElement(
                                                "h3",
                                                { "class": "font-accident-two-normal uppercase text-center", style: { textAlign: 'left', marginBottom: '40px' } },
                                                "Contact me directly"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Address:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                "No. 5, 8th Street, Padadshahr, Ahvaz, Iran"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Phone:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                "(+98) 935 722 0490"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Email:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                "esmat.zilaei@gmail.com"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Linkedin:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                React.createElement(
                                                    "a",
                                                    { href: "https://www.linkedin.com/in/esmat-zilaei-koozevaki-a48b79199/" },
                                                    "https://www.linkedin.com/in/esmat-zilaei-koozevaki-a48b79199/"
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "col-md-6",
                                        "data-animation-origin": "right",
                                        "data-animation-duration": "500",
                                        "data-animation-delay": "800",
                                        "data-animation-distance": "25px" },
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-12" },
                                            React.createElement(
                                                "h3",
                                                { "class": "font-accident-two-normal uppercase text-center", style: { textAlign: 'left' } },
                                                "Riahi Legal (Niousha Riahi Avocate)"
                                            ),
                                            React.createElement(
                                                "p",
                                                { style: { marginBottom: '10px' } },
                                                "or you can contact me via my agent at riahi legal"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Address:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                "2001 Boulevard Robert-Bourassa #1700, Montreal, Quebec H3A 2A6, Canada"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Phone:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                "(+1) 514 953 3570"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Email:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                "niousha5359@gmail.com"
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "row" },
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-2" },
                                            React.createElement(
                                                "span",
                                                { className: "font-accident-two-bold uppercase" },
                                                "Website:"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "col-sm-10" },
                                            React.createElement(
                                                "p",
                                                { className: "small" },
                                                React.createElement(
                                                    "a",
                                                    { href: "https://www.riahilegal.com/" },
                                                    "https://www.riahilegal.com/"
                                                )
                                            )
                                        )
                                    )
                                )
                            ),
                            React.createElement("div", { className: "dividewhite3" })
                        )
                    )
                )
            );
        }
    }]);

    return ContactContent;
}(React.Component);

var domContainer = document.querySelector('#containerBody');
ReactDOM.render(React.createElement(ContactContent, null), domContainer);