'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ProtfolioContent = function (_React$Component) {
   _inherits(ProtfolioContent, _React$Component);

   function ProtfolioContent(props) {
      _classCallCheck(this, ProtfolioContent);

      return _possibleConstructorReturn(this, (ProtfolioContent.__proto__ || Object.getPrototypeOf(ProtfolioContent)).call(this, props));
   }

   _createClass(ProtfolioContent, [{
      key: "render",
      value: function render() {
         return React.createElement(
            "div",
            { className: "content-wrap" },
            React.createElement(
               "div",
               { id: "portfolio", className: "inner-content" },
               React.createElement(
                  "section",
                  { id: "page-title", className: "inner-section ui-menu-color03" },
                  React.createElement(
                     "div",
                     { className: "container-fluid nopadding" },
                     React.createElement(
                        "h2",
                        { className: "font-accident-two-light color01 uppercase",
                           "data-animation-origin": "right",
                           "data-animation-duration": "400",
                           "data-animation-delay": "100",
                           "data-animation-distance": "50px" },
                        "Portfolio"
                     )
                  )
               ),
               React.createElement(
                  "section",
                  { id: "counts", className: "light inner-section bg-color02", "data-section": "counter" },
                  React.createElement(
                     "div",
                     { className: "container-fluid nopadding" },
                     React.createElement(
                        "div",
                        { className: "count-container row" },
                        React.createElement(
                           "div",
                           { className: "col-lg-3 col-sm-6 col-xs-12 count" },
                           React.createElement(
                              "div",
                              {
                                 "data-animation-origin": "top",
                                 "data-animation-duration": "300",
                                 "data-animation-delay": "200",
                                 "data-animation-distance": "35px" },
                              React.createElement(
                                 "div",
                                 { className: "count-icon" },
                                 React.createElement("i", { "class": "fa fa-chrome" })
                              ),
                              React.createElement(
                                 "span",
                                 { className: ".integers digit font-accident-two-normal web", "data-counter": "0" },
                                 "0"
                              ),
                              React.createElement(
                                 "div",
                                 { className: "count-text font-accident-two-bold" },
                                 "Websites"
                              )
                           )
                        ),
                        React.createElement(
                           "div",
                           { className: "col-lg-3 col-sm-6 col-xs-12 count" },
                           React.createElement(
                              "div",
                              {
                                 "data-animation-origin": "top",
                                 "data-animation-duration": "300",
                                 "data-animation-delay": "400",
                                 "data-animation-distance": "35px" },
                              React.createElement(
                                 "div",
                                 { className: "count-icon" },
                                 React.createElement("i", { className: "fa fa-mobile" })
                              ),
                              React.createElement(
                                 "span",
                                 { className: ".integers digit font-accident-two-normal mobile", "data-counter": "0" },
                                 "0"
                              ),
                              React.createElement(
                                 "div",
                                 { className: "count-text font-accident-two-bold" },
                                 "Mobile"
                              )
                           )
                        ),
                        React.createElement(
                           "div",
                           { className: "col-lg-3 col-sm-6 col-xs-12 count" },
                           React.createElement(
                              "div",
                              {
                                 "data-animation-origin": "top",
                                 "data-animation-duration": "300",
                                 "data-animation-delay": "600",
                                 "data-animation-distance": "35px" },
                              React.createElement(
                                 "div",
                                 { className: "count-icon" },
                                 React.createElement("i", { className: "fa fa-windows" })
                              ),
                              React.createElement(
                                 "span",
                                 { className: ".integers digit font-accident-two-normal windows", "data-counter": "0" },
                                 "0"
                              ),
                              React.createElement(
                                 "div",
                                 { className: "count-text font-accident-two-bold" },
                                 "Windows APP"
                              )
                           )
                        ),
                        React.createElement(
                           "div",
                           { className: "col-lg-3 col-sm-6 col-xs-12 count" },
                           React.createElement(
                              "div",
                              {
                                 "data-animation-origin": "top",
                                 "data-animation-duration": "300",
                                 "data-animation-delay": "800",
                                 "data-animation-distance": "35px" },
                              React.createElement(
                                 "div",
                                 { className: "count-icon" },
                                 React.createElement("i", { className: "fa fa-bars" })
                              ),
                              React.createElement(
                                 "span",
                                 { className: ".integers digit font-accident-two-normal all", "data-counter": "0" },
                                 "0"
                              ),
                              React.createElement(
                                 "div",
                                 { className: "count-text font-accident-two-bold" },
                                 "All"
                              )
                           )
                        )
                     ),
                     React.createElement("div", { className: "dividewhite2" })
                  )
               ),
               React.createElement(
                  "section",
                  { id: "portfolio-block", className: "inner-section", "data-section": "portfolio" },
                  React.createElement("div", { className: "dividewhite6" }),
                  React.createElement(
                     "div",
                     { id: "isotope-filters", className: "port-filter port-filter-light text-center",
                        "data-animation-origin": "top",
                        "data-animation-duration": "500",
                        "data-animation-delay": "200",
                        "data-animation-distance": "25px" },
                     React.createElement(
                        "ul",
                        null,
                        React.createElement(
                           "li",
                           null,
                           React.createElement(
                              "a",
                              { href: "#all", "data-filter": "*" },
                              "All"
                           )
                        ),
                        React.createElement(
                           "li",
                           null,
                           React.createElement(
                              "a",
                              { href: "#web", "data-filter": ".web" },
                              "Web"
                           )
                        ),
                        React.createElement(
                           "li",
                           null,
                           React.createElement(
                              "a",
                              { href: "#mobile", "data-filter": ".mobile" },
                              "Mobile"
                           )
                        ),
                        React.createElement(
                           "li",
                           null,
                           React.createElement(
                              "a",
                              { href: "#windows", "data-filter": ".windows" },
                              "Windows"
                           )
                        )
                     )
                  ),
                  React.createElement("div", { className: "dividewhite6" }),
                  React.createElement(
                     "div",
                     { className: "row masonry-row" },
                     React.createElement(
                        "div",
                        { className: "grid container-fluid text-center" },
                        React.createElement(
                           "div",
                           { id: "posts", className: "row popup-container" },
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "left",
                                       "data-animation-duration": "600",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "100px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "car" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/car/1.jpg", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/car/2.jpg", className: "gallery-item", title: "" }),
                                       React.createElement("a", { href: "./assets/images/car/3.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/car/4.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/car/5.jpg", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/car/logo.png", className: "img-responsive'", alt: "car" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Arvand Cars Management"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/car", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer mobile col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "carapp" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/carapp/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/carapp/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/5.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/6.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/7.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/8.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/9.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/10.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/11.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/12.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/13.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/carapp/14.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/carapp/logo.jpg", className: "img-responsive", alt: "carapp" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Arvand Cars Management App"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/carapp", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web seo col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "top",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "work" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/work/1.jpg", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/work/2.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/work/3.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/work/4.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/work/5.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/work/6.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/work/7.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/work/8.jpg", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/work/logo.jpg", className: "img-responsive", alt: "work" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Arvand Customs Management"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/work", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "gis" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/gis/1.jpg", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/gis/2.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/gis/3.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/gis/4.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/gis/5.jpg", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/gis/logo.jpg", className: "img-responsive", alt: "gis" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "GIS Map Management"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/gis", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer mobile col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "vocabulary" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/vocabulary/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/vocabulary/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/5.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/6.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/7.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/8.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/9.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/10.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/vocabulary/11.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/vocabulary/logo.jpg", className: "img-responsive", alt: "vocabulary" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Vocabulary App"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/vocabulary", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer mobile col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "arvandyar" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/arvandyar/1.jpg", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/arvandyar/2.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/arvandyar/3.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/arvandyar/4.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/arvandyar/5.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/arvandyar/6.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/arvandyar/7.jpg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/arvandyar/8.jpg", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/arvandyar/logo.png", className: "img-responsive", alt: "arvandyar" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Arvand Yar App"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/arvandyar", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "shandiz" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/shandiz/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/shandiz/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/shandiz/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/shandiz/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/shandiz/5.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/shandiz/logo.jpg", className: "img-responsive", alt: "shandiz" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Shandiz Web shop"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/shandiz", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "company" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/company/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/company/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/5.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/6.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/7.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/8.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/9.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/10.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/11.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/12.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/company/13.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/company/logo.jpg", className: "img-responsive", alt: "company" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Company Registration"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/company", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer windows col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "goranTransport" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/goranTransport/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/goranTransport/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/5.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/6.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/7.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/8.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/9.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/10.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/11.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/goranTransport/12.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/goranTransport/logo.jpg", className: "img-responsive", alt: "goranTransport" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Goran Transport"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/goranTransport", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer windows col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "solutionIt" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/solutionIt/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/solutionIt/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/5.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/6.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/7.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/8.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/9.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/10.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/11.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/12.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/13.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/solutionIt/14.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/solutionIt/logo.png", className: "img-responsive", alt: "solutionIt" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Solutions IT Dr"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/solutionIt", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "food" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/food/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/food/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/food/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/food/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/food/5.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/food/logo.jpg", className: "img-responsive", alt: "food" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Food Reservation"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/food", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "cineport" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/cineport/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/cineport/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/cineport/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/cineport/4.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/cineport/5.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/cineport/6.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/cineport/7.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/cineport/8.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/cineport/logo.jpg", className: "img-responsive", alt: "cineport" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Cineport"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/cineport", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer web col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "adic" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/adic/1.png", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/adic/2.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/adic/3.png", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/adic/4.png", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/adic/logo.jpg", className: "img-responsive", alt: "adic" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Adic"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/adic", href: "#" })
                                    )
                                 )
                              )
                           ),
                           React.createElement(
                              "div",
                              { className: "grid-item grid-sizer windows col-xs-6 col-sm-4 col-md-3" },
                              React.createElement(
                                 "div",
                                 { className: "item-wrap" },
                                 React.createElement(
                                    "figure",
                                    { className: "effect-goliath ui-menu-color02",
                                       "data-animation-origin": "right",
                                       "data-animation-duration": "400",
                                       "data-animation-delay": "400",
                                       "data-animation-distance": "50px" },
                                    React.createElement(
                                       "div",
                                       { className: "popup-call gallery-group-item", "data-group": "regioExpress" },
                                       React.createElement(
                                          "a",
                                          { href: "./assets/images/regioExpress/1.jpeg", className: "gallery-item" },
                                          React.createElement("i", { className: "flaticon-tool" })
                                       ),
                                       React.createElement("a", { href: "./assets/images/regioExpress/2.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/3.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/4.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/5.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/6.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/7.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/8.jpeg", className: "gallery-item" }),
                                       React.createElement("a", { href: "./assets/images/regioExpress/9.jpeg", className: "gallery-item" })
                                    ),
                                    React.createElement("img", { src: "./assets/images/regioExpress/logo.jpg", className: "img-responsive", alt: "regioExpress" }),
                                    React.createElement(
                                       "figcaption",
                                       null,
                                       React.createElement(
                                          "div",
                                          { className: "fig-description" },
                                          React.createElement(
                                             "h3",
                                             null,
                                             "Regio Express"
                                          )
                                       ),
                                       React.createElement("a", { "data-content": "protfolio/regioExpress", href: "#" })
                                    )
                                 )
                              )
                           )
                        )
                     )
                  ),
                  React.createElement("div", { className: "dividewhite8" })
               )
            )
         );
      }
   }]);

   return ProtfolioContent;
}(React.Component);

var domContainer = document.querySelector('#containerBody');
ReactDOM.render(React.createElement(ProtfolioContent, null), domContainer);