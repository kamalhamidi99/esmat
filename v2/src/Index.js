import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './assets/custom/2.2.2/css/preloader.css';
import './assets/cdn/bootstrap/3.3.7/css/bootstrap.min.css';
import './assets/cdn/font-awesome/4.7.0/css/font-awesome.min.css';
import './assets/vendor/flaticons/flaticon.min.css';
import './assets/vendor/hover/css/hover-min.css';
import './assets/vendor/mfp/css/magnific-popup.min.css';
import './assets/vendor/owl-carousel/assets/owl.carousel.min.css';
import './assets/custom/2.2.2/css/style.css';
import './assets/custom/2.2.2/css/colors/light.css';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);