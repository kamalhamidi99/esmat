import React from 'react';

class Contacts extends React.Component {
    render() {
        return (
            <div className="content-wrap">
                <div id="contacts" className="inner-content">
                    <section id="page-title" className="inner-section ui-menu-color04">
                        <div className="container-fluid nopadding">
                            <h2 className="font-accident-two-light color01 uppercase"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="100"
                                data-animation-distance="50px">Contacts</h2>
                            {/* <h4 className="font-accident-two-normal color01 uppercase subtitle"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="200"
                                data-animation-distance="50px">Made for Creative Professionals</h4>
                            <p className="small color01"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="300"
                                data-animation-distance="50px">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam id metus purus. Ut vulputate, est vel tincidunt mattis, orci neque iaculis lectus, et interdum quam felis vel tortor. Fusce ultrices dui quis nunc dignissim faucibus. Ut ac odio quis nibh viverra fringilla ac id nisi. Suspendisse tincidunt augue quis ligula cursus, non efficitur ligula faucibus. Mauris id neque maximus, tincidunt metus et, sodales nulla.
                            </p> */}
                        </div>
                    </section>
                    <section id="contacts-data" className="inner-block">
                        <div className="container-fluid nopadding">
                            <div className="dividewhite5"></div>
                            <div className="row">
                                <div className="col-md-6"
                                    data-animation-origin="right"
                                    data-animation-duration="500"
                                    data-animation-delay="500"
                                    data-animation-distance="25px">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h3 class="font-accident-two-normal uppercase text-center" style={{ textAlign: 'left', marginBottom: '40px' }}>Contact me directly</h3>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Address:</span></div>
                                        <div className="col-sm-10"><p className="small">No. 5, 8th Street, Padadshahr, Ahvaz, Iran</p></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Phone:</span></div>
                                        <div className="col-sm-10"><p className="small">(+98) 935 722 0490</p></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Email:</span></div>
                                        <div className="col-sm-10"><p className="small">esmat.zilaei@gmail.com</p></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Linkedin:</span></div>
                                        <div className="col-sm-10"><p className="small"><a href="https://www.linkedin.com/in/esmat-zilaei-koozevaki-a48b79199/">https://www.linkedin.com/in/esmat-zilaei-koozevaki-a48b79199/</a></p></div>
                                    </div>
                                </div>
                                <div className="col-md-6"
                                    data-animation-origin="right"
                                    data-animation-duration="500"
                                    data-animation-delay="800"
                                    data-animation-distance="25px">
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h3 class="font-accident-two-normal uppercase text-center" style={{ textAlign: 'left' }}>Riahi Legal (Niousha Riahi Avocate)</h3>
                                            <p style={{ marginBottom: '10px' }}>or you can contact me via my agent at riahi legal</p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Address:</span></div>
                                        <div className="col-sm-10"><p className="small">2001 Boulevard Robert-Bourassa #1700, Montreal, Quebec H3A 2A6, Canada</p></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Phone:</span></div>
                                        <div className="col-sm-10"><p className="small">(+1) 514 953 3570</p></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Email:</span></div>
                                        <div className="col-sm-10"><p className="small">niousha5359@gmail.com</p></div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm-2"><span className="font-accident-two-bold uppercase">Website:</span></div>
                                        <div className="col-sm-10"><p className="small"><a href="https://www.riahilegal.com/">https://www.riahilegal.com/</a></p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="dividewhite3"></div>
                        </div>
                    </section>
                    {/* <section id="contacts-map" className="inner-section" data-section="map"
                        data-animation-origin="top"
                        data-animation-duration="500"
                        data-animation-delay="500"
                        data-animation-distance="25px">
                        <div className="container-fluid nopadding">
                            <div>
                                <div id="gm-panel">
                                    <div id="mapId" className="bigmap"></div>
                                </div>
                            </div>
                        </div>
                        <div className="dividewhite8"></div>
                    </section> */}
                </div>
            </div>
        );
    }
}

export default Contacts;