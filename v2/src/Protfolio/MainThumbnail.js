import React from 'react';
import { Link } from 'react-router-dom';

export function MainThumbnail(props) {
    return (
        <div className={`grid-item grid-sizer ${props.type} col-xs-6 col-sm-4 col-md-3 col-lg-3`}>
            <div className="item-wrap">
                <figure className="effect-goliath ui-menu-color02" data-animation-origin="left" data-animation-duration="600" data-animation-delay="400" data-animation-distance="100px">
                    <img src={props.logo} className="img-responsive'" alt={props.page} />
                    <figcaption>
                        <div className="fig-description">
                            <h3>{props.title}</h3>
                        </div>
                        <Link to={`/protfolio/${props.page}`}>
                            <span></span>
                        </Link>
                    </figcaption>
                </figure>
            </div>
        </div>
    );
}
