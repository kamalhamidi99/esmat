import React from 'react';

class PopupGallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visibility: true,
            render: 'button',
            currentImage: 0,
        };
    }
    handleVisibility = (visibility) => {
        this.setState({ visibility, render: 'button', currentImage: 0 });
    };
    handleRender = () => {
        this.setState({ render: 'image', visibility: true });
    };
    changeNext = () => {
        if (this.props.images.length <= ((this.state.currentImage + 1)))
            return;
        this.setState({ currentImage: this.state.currentImage + 1 });
    };
    changePrev = () => {
        if (this.state.currentImage === 0)
            return;
        this.setState({ currentImage: this.state.currentImage - 1 });
    };
    render() {
        const btn = <button className="btn btn-default" onClick={this.handleRender}>View All</button>;
        if (this.state.render === 'button') {
            return btn;
        }
        else {
            let imgObj =
                this.props.images.find((o, index) => {
                    return index === this.state.currentImage;
                });
            const ratio = imgObj.width / imgObj.height;
            let height = imgObj.height > window.innerHeight ? (window.innerHeight - 40) : (imgObj.height - 40);
            let width = Math.round(ratio * height);
            if (width > window.innerWidth) {
                width = (window.innerWidth - 80);
                height = Math.round(width / ratio);
            }
            return (
                <React.Fragment>
                    {btn}
                    <div className="gallery" style={{ display: (this.state.visibility ? 'block' : 'none') }}>
                        <div className="content" style={{ width: `${width}px`, height: `${height}px` }}>
                            <button type="button" className="prev" onClick={this.changePrev}>
                                <i className="fa fa-arrow-left"></i>
                            </button>
                            <img alt="path" src={imgObj.path} width="100%" height="100%" />
                            <button type="button" className="next" onClick={this.changeNext}>
                                <i className="fa fa-arrow-right"></i>
                            </button>
                        </div>
                        <div className="bg" onClick={() => this.handleVisibility(false)}></div>
                    </div>
                </React.Fragment>
            );
        }
    }
}

export default PopupGallery;