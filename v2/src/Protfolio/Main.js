import React from 'react';
import { Items } from './Items';
import { MainThumbnail } from "./MainThumbnail";
import { MainCounter } from "./MainCounter";

export class Main extends React.Component {
    render() {
        return (
            <div className="content-wrap" >
                <div id="portfolio" className="inner-content">
                    <section id="page-title" className="inner-section ui-menu-color03">
                        <div className="container-fluid nopadding">
                            <h2 className="font-accident-two-light color01 uppercase" data-animation-origin="right" data-animation-duration="400" data-animation-delay="100" data-animation-distance="50px">Portfolio</h2>
                        </div>
                    </section>
                    <section id="counts" className="light inner-section bg-color02" data-section="counter">
                        <div className="container-fluid nopadding">
                            <div className="count-container row">
                                <MainCounter id="1" title="Websites" type="web" icon="chrome" />
                                <MainCounter id="2" title="Mobile" type="mobile" icon="mobile" />
                                <MainCounter id="3" title="Windows APP" type="windows" icon="windows" />
                                <MainCounter id="4" title="All" type="*" icon="bars" />
                            </div>
                            <div className="dividewhite2"></div>
                        </div>
                    </section>
                    <section id="portfolio-block" className="inner-section" data-section="portfolio">
                        <div className="dividewhite6"></div>
                        <div id="isotope-filters" className="port-filter port-filter-light text-center" data-animation-origin="top" data-animation-duration="500" data-animation-delay="200" data-animation-distance="25px">
                            <ul>
                                <li><a href="#all" data-filter="*" onClick={() => this.props.changeshow("*")}>All</a></li>
                                <li><a href="#web" data-filter=".web" onClick={() => this.props.changeshow("web")}>Web</a></li>
                                <li><a href="#mobile" data-filter=".mobile" onClick={() => this.props.changeshow("mobile")}>Mobile</a></li>
                                <li><a href="#windows" data-filter=".windows" onClick={() => this.props.changeshow("windows")}>Windows</a></li>
                            </ul>
                        </div>
                        <div className="dividewhite6"></div>
                        <div className="row masonry-row">
                            <div className="grid container-fluid text-center">
                                <div id="posts" className="row popup-container">
                                    {
                                        Items.map((x, i) => {
                                            if (this.props.show === "*") {
                                                return <MainThumbnail key={i} title={x.title} type={x.category.toLowerCase()} page={x.page} logo={x.logo} />;
                                            }
                                            else {
                                                if (x.category.toLowerCase() === this.props.show)
                                                    return <MainThumbnail key={i} title={x.title} type={x.category.toLowerCase()} page={x.page} logo={x.logo} />;
                                            }
                                            return "";
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="dividewhite8"></div>
                    </section>
                </div>
            </div>
        );
    }
}
