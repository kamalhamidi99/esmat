import { v4 as uuidv4 } from 'uuid';

const car = {
    key: uuidv4(),
    page: "car",
    title: "Arvand Cars Management",
    subtitle: "This system builds to manage all cars that registered in the Arvand free zone.",
    logo: require("../assets/images/car/logo.png"),
    cover: require("../assets/images/car/1.jpg"),
    images: [
        {
            path: require("../assets/images/car/1.jpg"),
            width: 1366,
            height: 1089
        },
        {
            path: require("../assets/images/car/2.jpg"),
            width: 1366,
            height: 1036
        },
        {
            path: require("../assets/images/car/3.jpg"),
            width: 1366,
            height: 1902
        },
        {
            path: require("../assets/images/car/4.jpg"),
            width: 1366,
            height: 1223
        },
        {
            path: require("../assets/images/car/5.jpg"),
            width: 1366,
            height: 773
        },
    ],
    category: "Web",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "Arvand Free Zone",
    url: "https://car.arvandfreezone.com",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "Stimulsoft",
        "Highcharts",
    ],
    description: "The system is designed to manage all the cars of the Arvand license plate and applicants can view or request all of their car related issues in their profile. More than 100,000 cars are in the system."
};

const carapp = {
    key: uuidv4(),
    page: "carapp",
    title: "Arvand Cars Management Mobile App",
    subtitle: "This application built for customers to register their cars in the Arvand free zone.",
    logo: require("../assets/images/carapp/logo.jpg"),
    cover: require("../assets/images/carapp/4.png"),
    images: [
        {
            path: require("../assets/images/carapp/1.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/2.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/3.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/4.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/5.png"),
            width: 1080,
            height: 2388,
        },
        {
            path: require("../assets/images/carapp/6.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/7.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/8.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/9.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/10.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/11.png"),
            width: 1080,
            height: 2076,
        },
        {
            path: require("../assets/images/carapp/12.png"),
            width: 1080,
            height: 1920,
        },
        {
            path: require("../assets/images/carapp/13.png"),
            width: 1080,
            height: 3220,
        },
        {
            path: require("../assets/images/carapp/14.png"),
            width: 1080,
            height: 3784,
        },
    ],
    category: "Mobile",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "Arvand Free Zone",
    url: "https://cafebazaar.ir/app/com.arvand.car/?l=fa",
    technology: [
        "Xamarin Forms",
        "C#",
    ],
    description: "This application is designed to facilitate access to the profile of the Arvand car management and allows you to view and request all of your car affairs. The app is made using Xamarin Forms and is used for both Android and iOS."
};

const work = {
    key: uuidv4(),
    page: "work",
    title: "Arvand Customs Management",
    subtitle: "",
    logo: require("../assets/images/work/logo.jpg"),
    cover: require("../assets/images/work/1.jpg"),
    images: [
        {
            path: require("../assets/images/work/1.jpg"),
            width: 1366,
            height: 1055
        },
        {
            path: require("../assets/images/work/2.jpg"),
            width: 1366,
            height: 877
        },
        {
            path: require("../assets/images/work/3.jpg"),
            width: 1366,
            height: 1117
        },
        {
            path: require("../assets/images/work/4.jpg"),
            width: 1366,
            height: 1000
        },
        {
            path: require("../assets/images/work/5.jpg"),
            width: 1366,
            height: 898
        },
        {
            path: require("../assets/images/work/6.jpg"),
            width: 1366,
            height: 898
        },
        {
            path: require("../assets/images/work/7.jpg"),
            width: 1366,
            height: 2007
        },
        {
            path: require("../assets/images/work/8.jpg"),
            width: 1366,
            height: 2446
        },
    ],
    category: "Web",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "Arvand Free Zone",
    url: "https://work.arvandfreezone.com",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "jQuery",
        "Stimulsoft"
    ],
    description: "The system is made up of two parts, one for managing free-zone exports and imports and the other for managing downtown stores. Customs procedures are designed, including definitive clearance, entry into the area, re-export, reference and temporary entry of goods into the system. In this system, Barzegans can carry out their customs duties and check all their incoming goods. Sellers can also use this system as a cash register and buy their merchandise from merchants and sell it in their stores."
};

const gis = {
    key: uuidv4(),
    page: "gis",
    title: "GIS Map Management",
    subtitle: "",
    logo: require("../assets/images/gis/logo.jpg"),
    cover: require("../assets/images/gis/1.jpg"),
    images: [
        {
            path: require("../assets/images/gis/1.jpg"),
            width: 1366,
            height: 892
        },
        {
            path: require("../assets/images/gis/2.jpg"),
            width: 1366,
            height: 892
        },
        {
            path: require("../assets/images/gis/3.jpg"),
            width: 1366,
            height: 1278
        },
        {
            path: require("../assets/images/gis/4.jpg"),
            width: 1366,
            height: 896
        },
        {
            path: require("../assets/images/gis/5.jpg"),
            width: 1366,
            height: 874
        },
    ],
    category: "Web",
    author: "Esmat Zilaei",
    client: "",
    url: "",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "jQuery",
        "Leaflet",
        "SharpMap",
        "GeoServer",
        "Stimulsoft",
    ],
    description: "The system is designed to manage geographic maps nationwide. It is possible to analyze the data entered in each region separately and manage them. The system is designed to monitor water and arid lands to facilitate their assessment."
};

const vocabulary = {
    key: uuidv4(),
    page: "vocabulary",
    title: "Vocabulary App",
    subtitle: "A mobile app to learn new English words.",
    logo: require("../assets/images/vocabulary/logo.jpg"),
    cover: require("../assets/images/vocabulary/1.png"),
    images: [
        {
            path: require("../assets/images/vocabulary/1.png"),
            width: 1080,
            height: 1920
        },
        {
            path: require("../assets/images/vocabulary/2.png"),
            width: 1080,
            height: 1920
        },
        {
            path: require("../assets/images/vocabulary/3.png"),
            width: 1080,
            height: 1920
        },
        {
            path: require("../assets/images/vocabulary/4.png"),
            width: 1080,
            height: 1920
        },
        {
            path: require("../assets/images/vocabulary/5.png"),
            width: 1080,
            height: 1920
        },
        {
            path: require("../assets/images/vocabulary/6.png"),
            width: 1080,
            height: 2226
        },
        {
            path: require("../assets/images/vocabulary/7.png"),
            width: 1080,
            height: 4756
        },
        {
            path: require("../assets/images/vocabulary/8.png"),
            width: 1080,
            height: 3846
        },
        {
            path: require("../assets/images/vocabulary/9.png"),
            width: 1080,
            height: 2212
        },
        {
            path: require("../assets/images/vocabulary/10.png"),
            width: 1080,
            height: 1920
        },
        {
            path: require("../assets/images/vocabulary/11.png"),
            width: 1080,
            height: 1920
        },
    ],
    category: "Mobile",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "myself..!",
    url: "",
    technology: [
        "Xamarin Forms",
        "C#",
    ],
    description: ""
};

const arvandyar = {
    key: uuidv4(),
    page: "arvandyar",
    title: "Arvand Yar App",
    subtitle: "",
    logo: require("../assets/images/arvandyar/logo.png"),
    cover: require("../assets/images/arvandyar/2.jpg"),
    images: [
        {
            path: require("../assets/images/arvandyar/1.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/2.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/3.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/4.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/5.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/6.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/7.jpg"),
            width: 382,
            height: 681
        },
        {
            path: require("../assets/images/arvandyar/8.jpg"),
            width: 382,
            height: 681
        },
    ],
    category: "Mobile",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "Arvand Free Zone",
    url: "https://cafebazaar.ir/app/com.freezonea.aya",
    technology: [
        "Xamarin",
        "C#",
    ],
    description: "'Arvand Yar' application software has been designed and implemented to receive electronic services in Arvand Free Zone, tourism services, investment services as well as to send public reports in the field of inspection."
};

const shandiz = {
    key: uuidv4(),
    page: "shandiz",
    title: "Shandiz Web shop",
    subtitle: "",
    logo: require("../assets/images/shandiz/logo.jpg"),
    cover: require("../assets/images/shandiz/1.png"),
    images: [
        {
            path: require("../assets/images/shandiz/1.png"),
            width: 1680,
            height: 1156
        },
        {
            path: require("../assets/images/shandiz/2.png"),
            width: 1680,
            height: 1219
        },
        {
            path: require("../assets/images/shandiz/3.png"),
            width: 1680,
            height: 1430
        },
        {
            path: require("../assets/images/shandiz/4.png"),
            width: 1680,
            height: 1171
        },
        {
            path: require("../assets/images/shandiz/5.png"),
            width: 1680,
            height: 1994
        },
    ],
    category: "Web",
    author: "Esmat Zilaei",
    client: "Solutions-It Drenth",
    url: "https://shandiz.nl/",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "jQuery",
    ],
    description: "This website is designed to sell a variety of small and large food items."
};

const company = {
    key: uuidv4(),
    page: "company",
    title: "Company Registration",
    subtitle: "",
    logo: require("../assets/images/company/logo.jpg"),
    cover: require("../assets/images/company/2.png"),
    images: [
        {
            path: require("../assets/images/company/1.png"),
            width: 1680,
            height: 1005
        },
        {
            path: require("../assets/images/company/2.png"),
            width: 1680,
            height: 984
        },
        {
            path: require("../assets/images/company/3.png"),
            width: 1680,
            height: 1005
        },
        {
            path: require("../assets/images/company/4.png"),
            width: 1680,
            height: 1046
        },
        {
            path: require("../assets/images/company/5.png"),
            width: 1680,
            height: 1417
        },
        {
            path: require("../assets/images/company/6.png"),
            width: 1680,
            height: 1236
        },
        {
            path: require("../assets/images/company/7.png"),
            width: 1680,
            height: 1236
        },
        {
            path: require("../assets/images/company/8.png"),
            width: 1680,
            height: 1236
        },
        {
            path: require("../assets/images/company/9.png"),
            width: 1680,
            height: 1236
        },
        {
            path: require("../assets/images/company/10.png"),
            width: 1680,
            height: 1755
        },
        {
            path: require("../assets/images/company/11.png"),
            width: 1680,
            height: 1065
        },
        {
            path: require("../assets/images/company/12.png"),
            width: 1680,
            height: 1783
        },
        {
            path: require("../assets/images/company/13.png"),
            width: 1680,
            height: 1065
        },
    ],
    category: "Web",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "Free Zones",
    url: "https://cr.freezones.ir",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "Stimulsoft",
        "Highcharts",
    ],
    description: "The system, which operates seamlessly across all free zones of the country, allows applicants to do all the company’s registration work. Due to the expansive relationship of this system with other organs of the country, it has accelerated the registration process of the company, and applicants can do all their tasks remotely."
};

const goranTransport = {
    key: uuidv4(),
    page: "goranTransport",
    title: "Goran Transport",
    subtitle: "",
    logo: require("../assets/images/goranTransport/logo.jpg"),
    cover: require("../assets/images/goranTransport/2.png"),
    images: [
        {
            path: require("../assets/images/goranTransport/1.png"),
            width: 261,
            height: 918
        },
        {
            path: require("../assets/images/goranTransport/2.png"),
            width: 804,
            height: 752
        },
        {
            path: require("../assets/images/goranTransport/3.png"),
            width: 1286,
            height: 1053
        },
        {
            path: require("../assets/images/goranTransport/4.png"),
            width: 1884,
            height: 875
        },
        {
            path: require("../assets/images/goranTransport/5.png"),
            width: 1915,
            height: 830
        },
        {
            path: require("../assets/images/goranTransport/6.png"),
            width: 1918,
            height: 768
        },
        {
            path: require("../assets/images/goranTransport/7.png"),
            width: 1913,
            height: 795
        },
        {
            path: require("../assets/images/goranTransport/8.png"),
            width: 1446,
            height: 787
        },
        {
            path: require("../assets/images/goranTransport/9.png"),
            width: 1453,
            height: 790
        },
        {
            path: require("../assets/images/goranTransport/10.png"),
            width: 1454,
            height: 794
        },
        {
            path: require("../assets/images/goranTransport/11.png"),
            width: 1471,
            height: 775
        },
        {
            path: require("../assets/images/goranTransport/12.png"),
            width: 1460,
            height: 771
        },
    ],
    category: "Windows",
    author: "Esmat Zilaei",
    client: "Solutions-It Drenthe",
    url: "",
    technology: [
        "WinForms",
        "C#",
        "",
        "",
        "",
    ],
    description: ""
};

const solutionIt = {
    key: uuidv4(),
    page: "solutionIt",
    title: "Solutions IT Dr",
    subtitle: "",
    logo: require("../assets/images/solutionIt/logo.png"),
    cover: require("../assets/images/solutionIt/7.png"),
    images: [
        {
            path: require("../assets/images/solutionIt/1.png"),
            width: 1398,
            height: 715
        },
        {
            path: require("../assets/images/solutionIt/2.png"),
            width: 920,
            height: 623
        },
        {
            path: require("../assets/images/solutionIt/3.png"),
            width: 1365,
            height: 714
        },
        {
            path: require("../assets/images/solutionIt/4.png"),
            width: 1021,
            height: 669
        },
        {
            path: require("../assets/images/solutionIt/5.png"),
            width: 1260,
            height: 527
        },
        {
            path: require("../assets/images/solutionIt/6.png"),
            width: 1267,
            height: 673
        },
        {
            path: require("../assets/images/solutionIt/7.png"),
            width: 1290,
            height: 603
        },
        {
            path: require("../assets/images/solutionIt/8.png"),
            width: 1188,
            height: 619
        },
        {
            path: require("../assets/images/solutionIt/9.png"),
            width: 1271,
            height: 613
        },
        {
            path: require("../assets/images/solutionIt/10.png"),
            width: 1139,
            height: 685
        },
        {
            path: require("../assets/images/solutionIt/11.png"),
            width: 1021,
            height: 567
        },
        {
            path: require("../assets/images/solutionIt/12.png"),
            width: 1226,
            height: 684
        },
        {
            path: require("../assets/images/solutionIt/13.png"),
            width: 1174,
            height: 606
        },
        {
            path: require("../assets/images/solutionIt/14.png"),
            width: 1233,
            height: 664
        },
    ],
    category: "Windows",
    author: "Esmat Zilaei",
    client: "Solutions-It Drenthe",
    url: "",
    technology: [
        "WinForms",
        "C#",
    ],
    description: ""
};

const food = {
    key: uuidv4(),
    page: "food",
    title: "Food Reservation",
    subtitle: "",
    logo: require("../assets/images/food/logo.jpg"),
    cover: require("../assets/images/food/1.png"),
    images: [
        {
            path: require("../assets/images/food/1.png"),
            width: 1366,
            height: 777
        },
        {
            path: require("../assets/images/food/2.png"),
            width: 1366,
            height: 1147
        },
        {
            path: require("../assets/images/food/3.png"),
            width: 1366,
            height: 625
        },
        {
            path: require("../assets/images/food/4.png"),
            width: 1366,
            height: 1095
        },
        {
            path: require("../assets/images/food/5.png"),
            width: 1366,
            height: 1453
        },
        {
            path: require("../assets/images/food/6.png"),
            width: 1366,
            height: 1191
        },
    ],
    category: "Web",
    author: "Esmat Zilaei & Kamal Hamidi",
    client: "Ahwaz University of Petroleum",
    url: "",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "jQuery",
    ],
    description: "The system is designed to accommodate students and faculty. View foods on the calendar and report features for ordered foods."
};

const cineport = {
    key: uuidv4(),
    page: "cineport",
    title: "Cineport",
    subtitle: "",
    logo: require("../assets/images/cineport/logo.jpg"),
    cover: require("../assets/images/cineport/1.png"),
    images: [
        {
            path: require("../assets/images/cineport/1.png"),
            width: 1365,
            height: 2209
        },
        {
            path: require("../assets/images/cineport/2.png"),
            width: 1366,
            height: 1448
        },
        {
            path: require("../assets/images/cineport/3.png"),
            width: 1366,
            height: 1635
        },
        {
            path: require("../assets/images/cineport/4.png"),
            width: 1366,
            height: 1134
        },
        {
            path: require("../assets/images/cineport/5.png"),
            width: 1366,
            height: 1542
        },
        {
            path: require("../assets/images/cineport/6.png"),
            width: 1366,
            height: 1153
        },
        {
            path: require("../assets/images/cineport/7.png"),
            width: 1366,
            height: 1110
        },
        {
            path: require("../assets/images/cineport/8.png"),
            width: 1366,
            height: 576
        },
        require("../assets/images/cineport/2.png"),
        require("../assets/images/cineport/3.png"),
        require("../assets/images/cineport/4.png"),
        require("../assets/images/cineport/5.png"),
        require("../assets/images/cineport/6.png"),
        require("../assets/images/cineport/7.png"),
        require("../assets/images/cineport/8.png"),
    ],
    category: "Web",
    author: "Esmat Zilaei",
    client: "",
    url: "",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
    ],
    description: ""
};

const adic = {
    key: uuidv4(),
    page: "adic",
    title: "Adic",
    subtitle: "",
    logo: require("../assets/images/adic/logo.jpg"),
    cover: require("../assets/images/adic/1.png"),
    images: [
        {
            path: require("../assets/images/adic/1.png"),
            width: 1680,
            height: 858
        },
        {
            path: require("../assets/images/adic/2.png"),
            width: 1680,
            height: 934
        },
        {
            path: require("../assets/images/adic/3.png"),
            width: 1680,
            height: 1233
        },
        {
            path: require("../assets/images/adic/4.png"),
            width: 1680,
            height: 907
        },
    ],
    category: "Web",
    author: "Esmat Zilaei",
    client: "Arvand Free Zone",
    url: "https://adic.arvandfreezone.com",
    technology: [
        "Asp.net Mvc",
        "C#",
        "Sql Server",
        "Bootstrap",
        "Stimulsoft",
    ],
    description: ""
};

const regioExpress = {
    key: uuidv4(),
    page: "regioExpress",
    title: "Regio Express",
    subtitle: "",
    logo: require("../assets/images/regioExpress/logo.jpg"),
    cover: require("../assets/images/regioExpress/2.jpeg"),
    images: [
        {
            path: require("../assets/images/regioExpress/1.jpeg"),
            width: 560,
            height: 466
        },
        {
            path: require("../assets/images/regioExpress/2.jpeg"),
            width: 1569,
            height: 919
        },
        {
            path: require("../assets/images/regioExpress/3.jpeg"),
            width: 1600,
            height: 776
        },
        {
            path: require("../assets/images/regioExpress/4.jpeg"),
            width: 1600,
            height: 757
        },
        {
            path: require("../assets/images/regioExpress/5.jpeg"),
            width: 1175,
            height: 756
        },
        {
            path: require("../assets/images/regioExpress/6.jpeg"),
            width: 1481,
            height: 670
        },
        {
            path: require("../assets/images/regioExpress/7.jpeg"),
            width: 1600,
            height: 660
        },
        {
            path: require("../assets/images/regioExpress/8.jpeg"),
            width: 1538,
            height: 691
        },
        {
            path: require("../assets/images/regioExpress/9.jpeg"),
            width: 790,
            height: 784
        },
    ],
    category: "Windows",
    author: "Esmat Zilaei",
    client: "Solutions-It Drenthe",
    url: "",
    technology: [
        "WinForms",
        "C#",
    ],
    description: ""
};

// const pagexxxx = {
//     key: uuidv4(),
//     page: "",
//     title: "",
//     subtitle: "",
//     logo: require(""),
//     cover: require(""),
//     images: [
//         require(""),
//     ],
//     category: "",
//     author: "",
//     client: "",
//     url: "",
//     technology: [
//         "",
//         "",
//         "",
//         "",
//         "",
//     ],
//     description: ""
// };

export const Items = [
    car, carapp, work, gis, vocabulary, arvandyar, shandiz,
    company, goranTransport, solutionIt, food, cineport,
    adic, regioExpress
]