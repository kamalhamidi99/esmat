import React from 'react';
import { Items } from './Items';
import { Page } from './Page';
import { Main } from "./Main";

class Protfolio extends React.Component {
    constructor() {
        super();
        this.state = {
            show: '*'
        };
    }
    handleChangeShow = (s) => {
        this.setState({
            show: s
        });
    };
    render() {
        if (this.props.match.params.page) {
            let page = Items.find((raw) => {
                return raw.page === this.props.match.params.page;
            });
            if (page !== undefined) {
                return <Page key={page.key} page={page} history={this.props.history} />;
            }
        }
        return <Main show={this.state.show} changeshow={this.handleChangeShow} />;
    }
}

export default Protfolio;