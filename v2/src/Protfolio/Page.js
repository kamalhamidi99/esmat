import React from 'react';
import { Items } from './Items';
import PopupGallery from './PopupGallery';

export class Page extends React.Component {
    gotonext = () => {
        const index = Items.indexOf(this.props.page);
        const next = Items[index + 1];
        if (next !== undefined) {
            this.props.history.push(`/protfolio/${next.page}`);
        }
    };
    gotoprev = () => {
        const index = Items.indexOf(this.props.page);
        const prev = Items[index - 1];
        if (prev !== undefined) {
            this.props.history.push(`/protfolio/${prev.page}`);
        }
    };
    componentDidMount = () => {
        const elmnt = document.getElementById("portfolio-item-page");
        elmnt.scrollIntoView();
    }
    render() {
        return (
            <div key={this.props.page.key} className="content-wrap">
                <div id="portfolio-item-page" className="inner-content" style={{ position: "relative" }}>
                    <div className="prevnext">
                        <button className="pf-prev" type="button" onClick={this.gotoprev}>prev</button>
                        <button className="pf-next" type="button" onClick={this.gotonext}>next</button>
                    </div>
                    <section className="inner-section">
                        <div className="post-header" data-animation-origin="right" data-animation-duration="400" data-animation-delay="100" data-animation-distance="50px">
                            <h2 className="font-accident-two-normal">{this.props.page.title}</h2>
                            <p>{this.props.page.subtitle}</p>
                            <div className="dividewhite1"></div>
                        </div>
                        <div className="dividewhite4"></div>
                        <div className="row">
                            <div className="col-md-4" data-animation-origin="top" data-animation-duration="400" data-animation-delay="400" data-animation-distance="50px">
                                <div className="row">
                                    <div className="col-md-12 col-sm-6 col-xs-12">
                                        <img src={this.props.page.cover} className="img-responsive radius-4" alt="portfolio item" />
                                        <div className="dividewhite2"></div>
                                    </div>
                                    <div className="col-md-12 col-sm-6 col-xs-12">
                                        <PopupGallery images={this.props.page.images} />
                                    </div>
                                </div>
                                <div className="dividewhite6"></div>
                            </div>
                            <div id="portfolio-overview" className="col-md-8" data-animation-origin="top" data-animation-duration="400" data-animation-delay="600" data-animation-distance="50px">
                                <h3 className="font-accident-two-light">Project Data</h3>
                                <div className="dividewhite4"></div>
                                <div className="portfolio-item-details table">
                                    <div className="row table-row">
                                        <div className="table-cell">Categories:</div>
                                        <div className="table-cell"><span>{this.props.page.category}</span></div>
                                    </div>
                                    <div className="row table-row">
                                        <div className="table-cell">Author:</div>
                                        <div className="table-cell"><span>{this.props.page.author}</span></div>
                                    </div>
                                    <div className="row table-row">
                                        <div className="table-cell">Client:</div>
                                        {this.props.page.client !== ""
                                            ? <div className="table-cell"><span>{this.props.page.client}</span></div>
                                            : "-"}
                                    </div>
                                    <div className="row table-row">
                                        <div className="table-cell">Url:</div>
                                        {this.props.page.url !== ""
                                            ? <div className="table-cell"><span><a href={this.props.page.url}>{this.props.page.url}</a></span></div>
                                            : "-"}
                                    </div>
                                </div>
                                <div className="dividewhite4"></div>
                                <p>
                                    {this.props.page.description}
                                </p>
                                <div className="dividewhite4"></div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <h3 className="font-accident-two-light">Technology</h3>
                                        <div className="dividewhite2"></div>
                                        <ul className="portfolio-item-details">
                                            {this.props.page.technology.map((x, i) => {
                                                return <li key={i}>{x}</li>;
                                            })}
                                        </ul>
                                        <div className="dividewhite4"></div>
                                    </div>
                                    <div className="col-md-8">
                                        <h3 className="font-accident-two-light">Share</h3>
                                        <div className="dividewhite2"></div>
                                        <div className="share">
                                            <ul className="list-inline social">
                                                <li><a href="#facebook" className="rst-icon-facebook"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#twitter" className="rst-icon-twitter"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#pinterest" className="rst-icon-pinterest"><i className="fa fa-pinterest"></i></a></li>
                                                <li><a href="#instagram" className="rst-icon-instagram"><i className="fa fa-instagram"></i></a></li>
                                                <li><a href="#google" className="rst-icon-google"><i className="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="dividewhite8"></div>
                    </section>
                </div>
            </div>
        );
    }
}

