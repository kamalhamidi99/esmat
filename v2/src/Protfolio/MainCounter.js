import React from 'react';
import { Items } from './Items';
import CountUp from 'react-countup';

export function MainCounter(props) {
    const value = Items.filter((x) => {
        if (props.type !== "*")
            return x.category.toLowerCase() === props.type;
        else
            return x.category.toLowerCase();
    }).length;
    return (
        <div className="col-lg-3 col-sm-6 col-xs-12 count">
            <div data-animation-origin="top" data-animation-duration="300" data-animation-delay="200" data-animation-distance="35px">
                <div className="count-icon">
                    <i className={`fa fa-${props.icon}`}></i>
                </div>
                <CountUp end={value} delay={0}>
                    {({ countUpRef }) => (
                        <div>
                            <span className={`.integers digit font-accident-two-normal ${props.type}`} ref={countUpRef} />
                        </div>
                    )}
                </CountUp>
                <div className="count-text font-accident-two-bold">{props.title}</div>
            </div>
        </div>
    );
}
