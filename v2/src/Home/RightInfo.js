import React from 'react';

export function RightInfo(props) {
    return (
        <span className={`col-md-6 flex-column bg-color0${props.bgcolor} p-grid-item nopadding hvr-bounce-to-top hvr-${props.color}`}>
            <div className="flex-panel padding-44">
                <div className={`p-icon ${props.color}`}></div>
                <span className="uppercase">{props.title}</span>
                <p>{props.description}</p>
            </div>
        </span>
    );
}
