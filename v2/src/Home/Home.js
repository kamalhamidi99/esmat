import React from 'react';
import { RightInfo } from './RightInfo';
import { ProgressCircle } from './ProgressCircle';
import { ProgressBar } from './ProgressBar';

const skills = [
    { title: "C#", value: "0.85", tocolor: "#51f2ec" },
    { title: "SQL", value: "0.80", tocolor: "#ffd200" },
    { title: "Javascript", value: "0.75", tocolor: "#F09C88" },
    { title: "HTML/CSS", value: "0.90", tocolor: "#5cb85c" },
    { title: "Bootstrap", value: "1", tocolor: "#563d7c" },
    { title: "Gitbal", value: "0.6", tocolor: "#e24329" },
    { title: "Highcharts", value: "0.95", tocolor: "#7a79a6" },
    { title: "Jquery", value: "0.82", tocolor: "#444444" },
    { title: "Photoshop", value: "0.95", tocolor: "#201e2b" },
    { title: "ReactJs", value: "0.85", tocolor: "#61dafb" },
    { title: "Stimulsoft", value: "0.90", tocolor: "#4b7fb9" },
    { title: "Xamarin", value: "0.70", tocolor: "#3498db" },
    { title: "Xd", value: "1", tocolor: "#fb3fbc" },
    { title: "Code", value: "0.90", tocolor: "#2ca2f1" },
    { title: "Asp.Net Mvc", value: "0.95", tocolor: "#00589b" },
    { title: "Asp.Net Core", value: "0.70", tocolor: "#5c2d91" },
    { title: "Ssms", value: "0.80", tocolor: "#ffb900" },
    { title: "Redis", value: "0.70", tocolor: "#e02d23" },
]

class Home extends React.Component {
    constructor() {
        super();
        this.state = {
            currentSlide: 0
        }
    }
    changeSlideNext = () => {
        if (skills.length <= ((this.state.currentSlide + 1) * 4))
            return
        const all = skills.filter((item, index) => {
            const page = this.state.currentSlide * 4;
            if (index >= page && index <= (page + 3))
                return item;
            return null;
        });
        if (all.length > 0) {
            const current = this.state.currentSlide;
            this.setState({
                currentSlide: current + 1
            });
        }
    }
    changeSlidePrev = () => {
        if (this.state.currentSlide === 0)
            return;
        const all = skills.filter((item, index) => {
            const page = this.state.currentSlide * 4;
            if (index >= page && index <= (page + 3))
                return item;
            return null;
        });
        if (all.length > 0) {
            const current = this.state.currentSlide;
            this.setState({
                currentSlide: current - 1
            });
        }
    }
    render() {
        return (
            <div className="content-wrap">
                <section className="container-fluid" data-section="home">
                    <div className="row flex-row">
                        <div id="personal-2" className="col-md-4 flex-column ui-block-color01 light nopadding"
                            data-animation-origin="right" data-animation-duration="300" data-animation-delay="600"
                            data-animation-distance="200px">
                            <div className="row flex-panel nopadding">
                                <div className="col-md-12 nopadding">
                                    <div className="row flex-row nopadding">
                                        <RightInfo color="green" bgcolor="2" title="Status" description="Open to offers" />
                                        <RightInfo color="blue" bgcolor="3" title="Desired Salary" description="To be Discussed" />
                                    </div>
                                </div>
                                <div className="col-md-12 nopadding">
                                    <div className="row flex-row nopadding">
                                        <RightInfo color="red" bgcolor="3" title="Job Type" description="Contract, Freelance" />
                                        <RightInfo color="yellow" bgcolor="2" title="Mobility" description="Ready to Relocate" />
                                    </div>
                                </div>
                                <div className="col-md-12 nopadding">
                                    <div className="row flex-row nopadding">
                                        <RightInfo color="blue" bgcolor="2" title="Position" description="Senior Programmer" />
                                        <RightInfo color="orange" bgcolor="3" title="Industry" description="Web Development" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="details" className="col-md-8 flex-column bg-color01 light nopadding" data-animation-origin="left"
                            data-animation-duration="300" data-animation-delay="400" data-animation-distance="200px">
                            <div className="padding-50 flex-panel">
                                <div className="row row-no-padding">
                                    <div className="col-md-12 nopadding">
                                        <h3 className="font-accident-two-normal uppercase">A little thing about me</h3>
                                        <div className="quote"></div>
                                    </div>
                                </div>
                                <div className="divider-dynamic"></div>
                                <div className="row nopadding">
                                    <div className="col-md-12 infoblock nopadding">
                                        <span>Hello, my name is Esmat. I'm 31 years old. I have 6 years of professional experience. I work as a web developer, also along the way, I learned to build windows and mobile applications.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="professional" className="container-fluid" data-section="home">
                    <div className="row flex-row">
                        <div id="pro-experience" className="col-md-4 flex-column dark nopadding ui-block-color02 flex-col"
                            data-animation-origin="bottom" data-animation-duration="300" data-animation-delay="800"
                            data-animation-distance="200px">
                            <div className="padding-50 flex-panel">
                                <h3 className="font-accident-two-normal uppercase">Employment</h3>
                                <div className="dividewhite4"></div>
                                <div className="experience">
                                    <ul className="">
                                        <li className="date">2016 - Present</li>
                                        <li className="company uppercase">
                                            <a href="https://www.solutions-it.nl/">
                                                Solutions-it Drenthe
                                            </a>
                                            <small style={{ fontSize: '10px' }}> (Netherlands) </small>
                                        </li>
                                        <li className="position">Programmer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Windows App & Web Developer
                                    </li>
                                    </ul>
                                    <ul className="">
                                        <li className="date">2014 - Present</li>
                                        <li className="company uppercase">
                                            <a href="http://tpe.co.ir/tejarat-pardazan-electronic/">
                                                Tejarat Pardazan Electronic
                                            </a>
                                            <small style={{ fontSize: '10px' }}> (Iran) </small>
                                        </li>
                                        <li className="position">Senior Programmer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Web Developer & Mobile Developer
                                    </li>
                                    </ul>
                                    <ul className="">
                                        <li className="date">2013 - 2014</li>
                                        <li className="company uppercase">
                                            <a href="#!">
                                                Pasha Sanat Yaran Company
                                            </a>
                                            <small style={{ fontSize: '10px' }}> (Iran) </small>
                                        </li>
                                        <li className="position">Programmer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Web Developer & Web Designer
                                        </li>
                                    </ul>
                                    <ul>
                                        <li className="date">2014 - Present</li>
                                        <li className="company uppercase">
                                            <a href="#!">
                                                Freelancer
                                            </a>
                                        </li>
                                        <li className="position">Developer</li>
                                        <li className="position" style={{ marginTop: '4px', fontStyle: 'italic', fontSize: '12px' }}>
                                            Web Developer & Mobile Developer
                                        </li>
                                    </ul>
                                </div>
                                <span className="btn btn-wh-trans btn-xs gototop">Learn More</span>
                                <div className="dividewhite1"></div>
                            </div>
                        </div>
                        <div id="bar-skills" className="col-md-8 flex-column dark nopadding ui-block-color03 flex-col"
                            data-section="bar-skills" data-animation-origin="right" data-animation-duration="400"
                            data-animation-delay="1100" data-animation-distance="200px">
                            <div className="row">
                                <div className="col-sm-12">
                                    <div className="padding-50 flex-panel" style={{ paddingBottom: '20px' }}>
                                        <h3 className="font-accident-two-normal uppercase">Professional skills</h3>
                                        <div className="dividewhite2"></div>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <ProgressBar title="Web Development" value="90" color="success" />
                                                <ProgressBar title="Mobile Development" value="70" color="warning" />
                                                <ProgressBar title="Windows Development" value="80" color="info" />
                                                <ProgressBar title="Designer" value="75" color="danger" />
                                                <div className="dividewhite1"></div>
                                                <div id="circle-skills" className="flex-column dark nopadding ui-block-color03 flex-col"
                                                    data-section="progress" data-animation-origin="right" data-animation-duration="400"
                                                    data-animation-delay="1100" data-animation-distance="200px">
                                                    <div className="row">
                                                        <div className="col-md-1">
                                                            <button type="button" onClick={this.changeSlidePrev} style={{ background: 'transparent', borderRadius: '3px', border: '1px solid #b7b7b7' }}>
                                                                <i className="fa fa-arrow-left"></i>
                                                            </button>
                                                        </div>
                                                        <div className="col-md-10">
                                                            {
                                                                skills.map((item, index) => {
                                                                    const page = this.state.currentSlide * 4;
                                                                    if (index >= page && index <= (page + 3))
                                                                        return (
                                                                            <ProgressCircle
                                                                                key={index}
                                                                                title={item.title}
                                                                                value={item.value}
                                                                                tocolor={item.tocolor} />
                                                                        )
                                                                    return null;
                                                                })
                                                            }
                                                        </div>
                                                        <div className="col-md-1">
                                                            <button type="button" onClick={this.changeSlideNext} style={{ background: 'transparent', borderRadius: '3px', border: '1px solid #b7b7b7' }}>
                                                                <i className="fa fa-arrow-right"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div >
        );
    }
}

export default Home;