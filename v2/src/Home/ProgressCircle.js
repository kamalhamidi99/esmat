import React from 'react';
import { buildStyles, CircularProgressbarWithChildren } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import { easeQuadInOut } from "d3-ease";
import AnimatedProgressProvider from "./Providers/AnimatedProgressProvider";

export function ProgressCircle(props) {
    return (
        <div className="col-md-3 col-sm-3 col-xs-6 nopadding">
            <div className="progressbar text-center">
                <AnimatedProgressProvider
                    valueStart={0}
                    valueEnd={props.value * 100}
                    duration={0.05}
                    easingFunction={easeQuadInOut}>
                    {value => {
                        return (
                            <CircularProgressbarWithChildren value={value}
                                styles={buildStyles({
                                    pathColor: props.tocolor,
                                    pathTransitionDuration: 0.5,
                                })}>
                                <span style={{ fontsize: '13px', display: 'block', color: 'black' }}>{props.title}</span>
                                <span style={{ fontsize: '10px', display: 'block', color: 'black' }}>{props.value * 100}%</span>
                            </CircularProgressbarWithChildren>
                        );
                    }}
                </AnimatedProgressProvider>
            </div>
        </div>
    );
}
