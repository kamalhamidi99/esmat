import React from 'react';

export class ProgressBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 0
        }
    }
    componentDidMount = () => {
        setTimeout(() => {
            this.setState({ width: this.props.value })
        }, 100);
    }
    render() {
        return (
            <div>
                <h5 className="font-accident-two-medium uppercase letter-spacing-03">{this.props.title}</h5>
                <div className="progress">
                    <div
                        className={`progress-bar progress-bar-${this.props.color}`}
                        role="progressbar"
                        style={{ width: `${this.state.width}%` }}>
                        {this.state.width}%
                        </div>
                </div>
            </div>
        );
    }
}

