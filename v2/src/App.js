import React from 'react';
import { Link, Route, Switch, BrowserRouter } from 'react-router-dom'
import Typical from 'react-typical'

import pdf from './assets/cv.pdf'

import Home from './Home/Home';
import Resume from './Resume';
import Feedback from './Feedback';
import Contacts from './Contacts';
import Protfolio from './Protfolio/Protfolio';
import Blog from './Blog/Blog'

class HeaderBody extends React.Component {
  render() {
    return (
      <header>
        <section id="top-navigation" className="container-fluid nopadding">
          <div className="row nopadding ident ui-bg-color01">
            <div className="col-md-4 vc-photo photo-04">&nbsp;</div>
            <div className="col-md-8 vc-name nopadding">
              <div className="row nopadding name">
                <div className="col-md-10 name-title">
                  <h1 className="font-accident-two-light">Esmat Zilaei Koozevaki</h1>
                </div>
                <div className="col-md-2 nopadding name-pdf">
                  <a href={pdf} className="hvr-sweep-to-right"><i className="flaticon-download149"
                    title="Download CV.pdf"></i></a>
                </div>
              </div>
              <div className="row nopadding position">
                <div className="col-md-10 position-title">
                  <section className="cd-intro">
                    <h2 className="cd-headline clip is-full-width font-accident-two-light">
                      <span>The experienced &nbsp;</span>
                      <span className="cd-words-wrapper">
                        <Typical
                          steps={['Web Developer', 1000, 'Mobile Developer', 1000, 'Windows App Developer', 1000]}
                          loop={Infinity}
                          wrapper="p"
                        />
                      </span>
                    </h2>
                  </section>
                </div>
                <div className="col-md-2 nopadding pdf">
                  <a href="#!" className="hvr-sweep-to-right"><i className="flaticon-behance7"
                    title="Language"></i></a>
                </div>
              </div>
              <ul id="nav" className="row nopadding cd-side-navigation">
                <li className="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color01" data-animation-duration="1000"
                  data-animation-delay="100">
                  <Link to="/">
                    <span className="hvr-sweep-to-bottom"><i
                      className="flaticon-insignia"></i><span>home</span></span>
                  </Link>
                </li>
                <li className="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color02" data-animation-duration="1000"
                  data-animation-delay="300">
                  <Link to="/resume">
                    <span className="hvr-sweep-to-bottom"><i
                      className="flaticon-graduation61"></i><span>resume</span></span>
                  </Link>
                </li>
                <li className="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color03" data-animation-duration="1000"
                  data-animation-delay="500">
                  <Link to="/protfolio">
                    <span className="hvr-sweep-to-bottom"><i
                      className="flaticon-book-bag2"></i><span>portfolio</span></span>
                  </Link>
                </li>
                <li className="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color04" data-animation-duration="1000"
                  data-animation-delay="700">
                  <Link to="/contacts">
                    <span className="hvr-sweep-to-bottom"><i
                      className="flaticon-placeholders4"></i><span>contacts</span></span>
                  </Link>
                </li>
                <li className="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color05" data-animation-duration="1000"
                  data-animation-delay="900">
                  <Link to="/feedback">
                    <span className="hvr-sweep-to-bottom"><i
                      className="flaticon-earphones18"></i><span>feedback</span></span>
                  </Link>
                </li>
                <li className="col-xs-4 col-sm-2 nopadding menuitem ui-menu-color06" data-animation-duration="1000"
                  data-animation-delay="1100">
                  <Link to="/blog">
                    <span className="hvr-sweep-to-bottom"><i
                      className="flaticon-pens15"></i><span>blog</span></span>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </section>
      </header>
    );
  }
}

class FooterBody extends React.Component {
  render() {
    return (
      <footer className="padding-50 dark ui-bg-color01" data-animation-origin="top" data-animation-duration="500"
        data-animation-delay="800" data-animation-distance="50px">
        <div className="container-fluid nopadding">
          <div className="row">
            <div className="col-sm-3 cv-link">
              <h5 className="font-accident-two-normal uppercase">Download cv</h5>
              <div className="dividewhite1"></div>
              <a href={pdf}><i className="fa fa-long-arrow-down" aria-hidden="true"></i>English</a>
              {/*  <a href="#!"><i className="fa fa-long-arrow-down" aria-hidden="true"></i>Franch</a>
              <a href="#!"><i className="fa fa-long-arrow-down" aria-hidden="true"></i>Persian</a> */}
              <p className="inline-block">
                The CV is in .pdf format. Use the Adobe Reader to open it.
                      </p>
              <div className="divider-dynamic"></div>
            </div>
            <div className="col-sm-3">
              <h5 className="font-accident-two-normal uppercase">Follow me</h5>
              <div className="follow">
                <ul className="list-inline social">
                  <li><a href="https://www.linkedin.com/in/esmat-zilaei-koozevaki-a48b79199/"
                    className="rst-icon-linkedin"><i className="fa fa-linkedin"></i></a></li>
                  <li><a href="#!" className="rst-icon-facebook"><i className="fa fa-facebook"></i></a></li>
                  <li><a href="#!" className="rst-icon-twitter"><i className="fa fa-twitter"></i></a></li>
                  <li><a href="#!" className="rst-icon-pinterest"><i className="fa fa-pinterest"></i></a></li>
                  <li><a href="#!" className="rst-icon-instagram"><i className="fa fa-instagram"></i></a></li>
                  <li><a href="#!" className="rst-icon-youtube"><i className="fa fa-youtube"></i></a></li>
                </ul>
              </div>
              <div className="divider-dynamic"></div>
            </div>
          </div>
          <div className="dividewhite1"></div>
          <div className="row">
            <div className="col-md-12 copyrights">
              <p>© 2020 Esmat Zilaei Koozevaki</p>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

function App() {
  return (
    <BrowserRouter>
      <HeaderBody />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/resume" component={Resume} />
        <Route exact path="/protfolio" component={Protfolio} />
        <Route path="/protfolio/:page" component={Protfolio} />
        <Route exact path="/contacts" component={Contacts} />
        <Route exact path="/feedback" component={Feedback} />
        <Route exact path="/blog" component={Blog} />
        <Route exact path="/blog/:page" component={Blog} />

        <Route component={Home} />
      </Switch>
      <FooterBody />
    </BrowserRouter>
  );
}

export default App;
