
const mytext = `using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WhatToCook.Common;
using WhatToCook.Web.Areas.Admin.Base;
using Microsoft.AspNetCore.Mvc;
using WhatToCook.Services.Entity.Diet.Diet;
using WhatToCook.Services.Entity.Diet.Diet.Objects;
using WhatToCook.Services.Entity.Diet.DietDailyTranslate;
using WhatToCook.Services.Entity.Diet.DietDailyTranslate.Objects;
using WhatToCook.Services.Entity.Diet.DietTranslate;
using WhatToCook.Services.Entity.Diet.DietTranslate.Objects;

namespace WhatToCook.Web.Areas.Admin.Features.Diet
{
    [AuthorizeTitle(nameof(DietController))]
    public class DietController : AdminBaseController
    {
        private readonly IDietService _dietService;
        private readonly IDietTranslateService _dietTranslateService;
        private readonly IDietDailyTranslateService _dietDailyTranslateService;

        public DietController(
            IDietService dietService,
            IDietTranslateService dietTranslateService,
            IDietDailyTranslateService dietDailyTranslateService)
        {
            _dietService = dietService;
            _dietTranslateService = dietTranslateService;
            _dietDailyTranslateService = dietDailyTranslateService;
        }

        #region Index

        [AuthorizeTitle(nameof(Index))]
        public async Task<IActionResult> Index()
        {
            var viewModel = new DietViewModel()
            {
                DietDtos = (await _dietService.GetAllAsync()).List,
            };

            viewModel.DietTranslateDtos =
                (await _dietTranslateService.GetAllAsync(search: new DietTranslateSearch()
                {
                    DietIds = viewModel.DietDtos.Select(x => x.Id).ToList()
                })).List;

            return View(viewModel);
        }

        #endregion

        #region Details

        [AuthorizeTitle(nameof(Details))]
        public async Task<IActionResult> Details(Guid id)
        {
            var dietDto = await _dietService.GetAsync(search: new DietSearch() { Id = id });
            if (dietDto == null)
                return NotFound();

            var viewModel = new DietViewModel
            {
                DietDto = dietDto,
                DietTranslateDto =
                    await _dietTranslateService.GetAsync(
                        search: new DietTranslateSearch()
                        {
                            LanguageModelId = BaseLanguageId, DietId = dietDto.Id
                        }),
                DietDailyTranslateDto = await _dietDailyTranslateService.GetAsync(
                    search: new DietDailyTranslateSearch()
                    {
                        LanguageModelId = BaseLanguageId, DietId = dietDto.Id
                    })
            };

            return View(viewModel);
        }

        #endregion

        #region Add

        [AuthorizeTitle(nameof(Add))]
        public IActionResult Add()
        {
            var viewModel = new DietViewModel()
            {
                DietDto = new DietDto(),
                DietTranslateDtos = new List<DietTranslateDto>(),
            };

            return View(viewModel);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Add(
            [Bind(Prefix = nameof(DietDto))]DietDto dto,
            IList<DietTranslateDto> dietTranslateDto = null,
            IList<DietDailyTranslateDto> dietDailyTranslateDto = null)
        {
            var result = await _dietService.AddAsync(dto);
            if (!result.Status)
            {
                ModelState.AddModelError(string.Empty, result.Message);
                return JsonResult(result);
            }

            await _dietTranslateService.ModifyAsync(Guid.Parse(result.Message), dietTranslateDto);
            await _dietDailyTranslateService.ModifyAsync(Guid.Parse(result.Message), dietDailyTranslateDto);

            return JsonResult(result);
        }

        #endregion

        #region Edit

        [AuthorizeTitle(nameof(Edit))]
        public async Task<IActionResult> Edit(Guid id)
        {
            var dietDto = await _dietService.GetAsync(search: new DietSearch() { Id = id });
            if (dietDto == null)
                return NotFound();

            var viewModel = new DietViewModel
            {
                DietDto = dietDto,
                DietTranslateDtos = (await _dietTranslateService.GetAllAsync(search: new DietTranslateSearch()
                {
                    DietId = dietDto.Id
                })).List,
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(
            [Bind(Prefix = nameof(DietDto))]DietDto dto,
            IList<DietTranslateDto> dietTranslateDto = null,
            IList<DietDailyTranslateDto> dietDailyTranslateDto = null)
        {
            var diet = await _dietService.GetAsync(search: new DietSearch()
            {
                Id = dto.Id
            });
            if (diet == null)
            {
                return NotFound();
            }

            diet.Name = dto.Name;

            var result = await _dietService.UpdateAsync(diet);
            if (!result.Status)
            {
                ModelState.AddModelError(string.Empty, result.Message);
                return JsonResult(result);
            }

            await _dietTranslateService.ModifyAsync(diet.Id, dietTranslateDto);
            await _dietDailyTranslateService.ModifyAsync(diet.Id, dietDailyTranslateDto);

            return JsonResult(result);
        }

        #endregion

        #region Delete

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeTitle(nameof(Delete))]
        public async Task<IActionResult> Delete(Guid id)
        {
            var diet = await _dietService.GetAsync(search: new DietSearch() { Id = id });
            if (diet == null)
                return NotFound();

            var result = await _dietService.DeleteAsync(id);
            return JsonResult(result);
        }

        #endregion

    }
}`;

export default mytext;