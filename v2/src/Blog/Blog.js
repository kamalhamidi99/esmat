import React from 'react';
import Main from "./Main";
import BlogAspNetCore from './BlogAspNetCore'

class Blog extends React.Component {
    constructor() {
        super();
        this.state = {
            show: '*'
        };
    }
    handleChangeShow = (s) => {
        this.setState({
            show: s
        });
    };
    componentDidMount = () => {
        const elmnt = document.getElementById("blog");
        elmnt.scrollIntoView();
    }
    render() {
        if (this.props.match.params.page) {
            switch (this.props.match.params.page) {
                case 'aspnetcore':
                    return <BlogAspNetCore history={this.props.history} />;
                default:
                    return <div></div>
            }
        }
        return <Main show={this.state.show} changeshow={this.handleChangeShow} />;
    }
}

export default Blog;