import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import * as all from 'react-syntax-highlighter/dist/esm/styles/hljs';
import codeString from './csharp'


class BlogAspNetCore extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showCode1: false
        }
    }
    handleShowCode1 = () => {
        this.setState((prev) => {
            return {
                showCode1: !prev.showCode1
            }
        })
    }
    render() {
        const s = all.xcode
        return (
            <div className="content-wrap">
                <div id="blog" className="inner-content" style={{ position: "relative" }}>
                    <section className="inner-section">
                        <div className="post-header" data-animation-origin="right" data-animation-duration="400" data-animation-delay="100" data-animation-distance="50px">
                            <h2 className="font-accident-two-normal">My Asp.Net Core Solution</h2>
                            <p>A little about my personal solution</p>
                            <div className="dividewhite1"></div>
                        </div>
                        <div className="dividewhite4"></div>
                        <div className="row">
                            <div className="col-md-4" data-animation-origin="top" data-animation-duration="400" data-animation-delay="400" data-animation-distance="50px">
                                <div className="row">
                                    <div className="col-md-12 col-sm-6 col-xs-12">
                                        <img src={require('../assets/imgblogs/aspnetcore.jpg')} className="img-responsive radius-4" alt="portfolio item" />
                                        <div className="dividewhite2"></div>
                                    </div>
                                    <div className="col-md-12 col-sm-6 col-xs-12">
                                        <h3 className="font-accident-two-light">Share</h3>
                                        <div className="dividewhite2"></div>
                                        <div className="share">
                                            <ul className="list-inline social">
                                                <li><a href="#facebook" className="rst-icon-facebook"><i className="fa fa-facebook"></i></a></li>
                                                <li><a href="#twitter" className="rst-icon-twitter"><i className="fa fa-twitter"></i></a></li>
                                                <li><a href="#pinterest" className="rst-icon-pinterest"><i className="fa fa-pinterest"></i></a></li>
                                                <li><a href="#instagram" className="rst-icon-instagram"><i className="fa fa-instagram"></i></a></li>
                                                <li><a href="#google" className="rst-icon-google"><i className="fa fa-google-plus"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div className="dividewhite6"></div>
                            </div>
                            <div id="blog-overview" className="col-md-8" data-animation-origin="top" data-animation-duration="400" data-animation-delay="600" data-animation-distance="50px">
                                <div>
                                    <p>
                                        To start a new project, I usually use my Solution that I designed in advance. In this Solution, I try to include all the needs that I may need to start a new project. Even if I needed a new feature along the way, I would put that feature in my Solution so that I could easily use it later if I needed it again.                                </p>
                                    <p>
                                        I use multi-layered architecture in my Solution. I will not talk about this architecture, but instead, I'll talk about the features in the Solution.                                </p>
                                    <p>
                                        The Solution designed to support multi-language, and right now supports English and Persian by default.                                </p>
                                    <p>
                                        Have you ever had too many controllers? Well, areas can be a good way to control this problem, but again, going up and down in the Controller, Models, and Views folders were annoying for me, so I put Feature Folders in the Solution, which allows me to manages them faster.                                </p>
                                    <p>
                                        One of the most useful features of this Solution is its user management system. Back in time, When I was using MVC, managing user access using Role was inefficient, so I've built a custom user access systems based on area, controller, and action ever since. Fortunately, the same system has implemented in Core, but in any case, I also personalized this system to make it better and more efficient.                                </p>
                                    <p>
                                        In this solution, not only it's possible to create an infinite number of roles, but also the users can have an infinitive number of them. And without using them, each user can have specific access to the system's capabilities according to the admin's taste.
                                    </p>
                                    <p>
                                        In this Solution, not only it's possible to create an infinite number of roles, but also the users can have an infinitive number of them. And without using them, each user can have specific access to the system's capabilities according to the admin's taste.                                </p>
                                    <p>
                                        To make the user management system more functional, I created several tag helpers, That can build the user side views faster, also, they can control access to the different parts of the system better than humans and without mistakes.                                </p>
                                    <p>
                                        I designed the data layer in a way that I can easily use a different type of database server. For now, it supports the SQL Server, SQL Server LocalDb, and SQLite. Changing between database servers is done by a simple line of option in `appsettings.json` file.                                </p>
                                    <p>
                                        In the Common layer, there are a lot of extension classes that I build over my entire development. Most of the time, I used them because they are handy and satisfy my needs. The Common layer itself is a NuGet package, which allows me to update my Solution easily.
                                </p>
                                </div>
                                <div className="dividewhite2"></div>
                                <div className="row" style={{
                                    padding: 10,
                                    border: '1px solid #ddd',
                                    borderRadius: 7
                                }}>
                                    <button onClick={() => this.handleShowCode1()}>
                                        {this.state.showCode1 ? 'Hide' : 'Show'} Code</button>
                                    <span style={{ marginLeft: 10 }}>DietController </span>
                                    <div style={{
                                        display: this.state.showCode1 ? 'flex' : 'none',
                                        marginTop: 10,
                                        marginBottom: -10
                                    }}>
                                        <SyntaxHighlighter language="csharp" style={s}>
                                            {codeString}
                                        </SyntaxHighlighter>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="dividewhite8"></div>
                    </section>
                </div>
            </div>
        );
    }
}

export default BlogAspNetCore