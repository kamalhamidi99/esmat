import React from 'react';
import { Link } from 'react-router-dom';

class Main extends React.Component {
    render() {
        return (
            <div className="content-wrap">
                <div id="blog" className="inner-content" style={{ minHeight: 'auto' }}>
                    <section id="page-title" className="inner-section ui-menu-color06">
                        <div className="container-fluid nopadding">
                            <h2 className="font-accident-two-light color01 uppercase"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="100"
                                data-animation-distance="50px">Blog</h2>
                            {/*  <h4 className="font-accident-two-normal color01 uppercase subtitle"
                                data-animation-origin="right"
                                data-animation-duration="400"
                                data-animation-delay="200"
                                data-animation-distance="50px">Coming Soon</h4> */}
                        </div>
                    </section>
                    <section class="inner-section " data-section="blog">
                        <div class="dividewhite4"></div>
                        <div id="isotope-filters" class="port-filter port-filter-light text-center" data-animation-origin="top" data-animation-duration="400" data-animation-delay="300" data-animation-distance="10px" style={{ visibility: 'visible' }}>
                            <ul>
                                <li><a href="#all" data-filter="*">All Categories</a></li>
                                {/* <li><a href="#design" data-filter=".design">Design</a></li>
                                <li><a href="#branding" data-filter=".branding">Branding</a></li> */}
                            </ul>
                        </div>
                        <div class="dividewhite6"></div>
                        <div class="row masonry-row">
                            <div class="grid container-fluid text-center" style={{ position: 'relative' }}>
                                <div id="posts" class="row popup-container">
                                    <div class="grid-item branding design col-md-3 col-sm-4" style={{ border: '1px solid #ddd', paddingTop: '15px', borderRadius: '7px', boxShadow: '0 0 5px #aaa' }}>
                                        <div class="item-wrap">
                                            <figure data-animation-origin="right" data-animation-duration="500" data-animation-delay="300" data-animation-distance="0px" style={{ visibility: 'visible' }}>
                                                <img src={require('../assets/imgblogs/aspnetcore.jpg')} class="img-responsive" alt="img11" />
                                                <figcaption>
                                                    <div class="post-meta"><span>by Esmat Zilaei</span></div>
                                                    <div class="post-header">
                                                        <h5>
                                                            <Link to={`/blog/aspnetcore`}>
                                                                My Asp.Net Core Solution
                                                            </Link>
                                                        </h5>
                                                    </div>
                                                    <div class="post-entry">
                                                        <p>In this blog, I will talk about my customized asp.net core solution that I built for my new project.</p>
                                                    </div>
                                                    {/* <div class="post-tag pull-left">
                                                        <span><a href="#branding" data-filter=".branding">Branding</a>,</span><span><a href="#design" data-filter=".design">Design</a></span>
                                                    </div> */}
                                                    <div class="post-more-link pull-right">
                                                        <Link to={`/blog/aspnetcore`}>More<i class="fa fa-long-arrow-right right"></i></Link>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* <div class="dividewhite1"></div> */}
                    </section>
                </div>
            </div >
        );
    }
}

export default Main